#include <iostream>
#include <list>
#include <iomanip>
#include <vector>
#include <fstream>
#include "stdafx.h"
#include <map>
#include <assert.h>
#include <string.h>
#include "nautyinit.h"
#include <cstdlib>
#include <time.h>
#include <cstdlib>
#include <string>
#define DIRNAME "graphviz"
using namespace std;

static bool original = true;
typedef uint64 graph64;
static TStr s_nemoCount = NULL;
static long long mtimeGraphviz = 0;
static long long mtime4 = 0;
static int maxId = 0;
static double totalTime = 0;
static int nemoCountTime = 0;
static vector< int > labelNodeIdCountArr;
static std::map<int, vector<int> > LabelNodeIdMap;
static vector<int> nodeIds;
typedef unsigned long vertex;
static int randTime = 0; //total time for random generation in seconds
static int randMsTotal = 0; //total time for random generation in ms
static int probIter = 0;
static int labelIter = 0;
static int totalGraphs = 0;
static std::vector<graph64> labelId;
static uint64 subgraphCounter = 0;  
static std::map<graph64, map<int, int> > nodeIdMap;
typedef std::vector<uint64> ResultCount;
static int labelCountPtr = 0;
typedef std::map< graph64, ResultCount > LabelCountMap; 
static LabelCountMap labelMap;
static float count= 0;
static int networks = 0;
static int callCount = 0;
static int randCount = 0;
static TStr inFile = NULL;
static TStr s_graphviz = NULL;
int randCallCount = 0;
static int randomCounter = 0;
static int networkCount = 0;
static TStr hashType = NULL;
bool randGraph = false;
TVec<TInt> prob;
static TVec<TInt> countSubgr;
static int countSubgrPtr = 0;
static int totalSubgr = 0;
static TVec<TFlt> concentration;
static TVec<TInt> randomIds;
static vector< vector<int> > labelNodeIdVec;
string randOutput = "nemoCounting.txt";
string outputfile = "nemoProfile.txt";
//string subgraphfile = "LabelNodeHash.txt";
//string graphFile = "graphImages.png";
//std::ofstream outfile(outputfile.c_str());
std::ofstream *outfile = NULL;
std::ofstream randOutfile(randOutput.c_str()); 
//std::ofstream randOutfile; 
//std::ofstream graphOutfile(graphFile.c_str(), ios::out | ios::app );
std::ofstream idOutfile(outputfile.c_str()); 
static TStr OutFNm =NULL;
struct timeval start, start1, start2, start3, start4, end, end1, end2, end3, end4;
typedef std::map< graph64, std::vector< std::vector<int> > > LabelNodeMap; 
static LabelNodeMap subgraphIds;

struct subgr_res
{
    graph64 id;
    double freq;
    double randMean;
    double randSd;
    double pValue;
    double zScore;
};


//BEGIN HELPER FUNCTIONS////////////////////
//prints vector to stdout
void printVector( const TVec<TInt>& vector)
{
    for(int i = 0; i < vector.Len(); i++)
    {
       cerr <<  vector[i] << " ";    
       randOutfile <<  vector[i] << " ";    
    }
    cerr <<  endl;
   // randOutfile <<  endl;
}


//checks vector for node id: returns true if found
bool TVecContains( const TVec<TInt>& vec, const int id)
{
    for(int j = 0; j <  vec.Len(); j++)
    {
        if(id == vec[j])
            return true;            
    }   

    return false;
}

//intializes 2d array to null
void init2dArr(int** arr, const int size)
{

    for( int i=0; i<size; ++i)
    {
        arr[i] = new int[size];
    } 
    
    //init neighborArr to zero
    for(int i = 0; i < size; i++)
    {
        for(int j = 0; j < size; j++)
        {
            arr[i][j] = 0;
        } 
    }
}

void initNodeIds(vector<int>& nodeIds, int numNodes)
{
    for(int i = 0; i <= numNodes; i++)
    {
        nodeIds.push_back(0);
    }
}

TVec<TInt> convertIntVec(vector<int> vec)
{
    TVec<TInt> subgraphs;
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        subgraphs.Add(vec[i]);
    }
    return subgraphs;
}

//END HELPER FUNCTIONS////////////////////

//SNAP function without printf statements
PUNGraph dirMethod(const TIntV& DegSeqV, TRnd& Rnd)
{

    const int Nodes = DegSeqV.Len();
     PUNGraph GraphPt = TUNGraph::New();
     TUNGraph& Graph = *GraphPt;
    Graph.Reserve(Nodes, -1);
    TIntV NIdDegV(DegSeqV.Len(), 0);
   int DegSum=0, edges=0;
   for (int node = 0; node < Nodes; node++) {
        Graph.AddNode(node);
    for (int d = 0; d < DegSeqV[node]; d++) { NIdDegV.Add(node); }
   DegSum += DegSeqV[node];
   }
   NIdDegV.Shuffle(Rnd);
  TIntPrSet EdgeH(DegSum/2); // set of all edges, is faster than graph edge lookup
     int u=0, v=0;
     for (int c = 0; NIdDegV.Len() > 1; c++) {
       u = Rnd.GetUniDevInt(NIdDegV.Len());
       while ((v = Rnd.GetUniDevInt(NIdDegV.Len())) == u) { }
       if (u > v) { Swap(u, v); }
       const int E1 = NIdDegV[u];
       const int E2 = NIdDegV[v];
       if (v == NIdDegV.Len()-1) { NIdDegV.DelLast(); } 
       else { NIdDegV[v] = NIdDegV.Last();  NIdDegV.DelLast(); }
       if (u == NIdDegV.Len()-1) { NIdDegV.DelLast(); } 
       else { NIdDegV[u] = NIdDegV.Last();  NIdDegV.DelLast(); }
       if (E1 == E2 || EdgeH.IsKey(TIntPr(E1, E2))) { continue; }
       EdgeH.AddKey(TIntPr(E1, E2));
       Graph.AddEdge(E1, E2);
       edges++;
     }
     return GraphPt;
}

graph64 getLabel(graph* canon, set* gv, const int G_N, const int G_M)
{

    //Fanmod code to retrieve canon data
    graph64 res_gr = 0ULL;
    for (int a = 0; a < G_N; ++a) 
    {
        gv = GRAPHROW(canon, a, G_M);
	    for (int b = 0; b < G_N; ++b) 
        {
		    res_gr <<= 1;
		    if (ISELEMENT(gv, b))
            {
		        res_gr |= 1;
		    }
	    }
    }
    return res_gr;
}

void initResultVec(ResultCount& vec)
{
    for(int i = 0; i < totalGraphs; i++)
    {
        vec.push_back(0);
    }
}


//fanmod code to determine id printing width
//get number of digits of a unint64
int numDigits(uint64 i)
{
    int ret = 1;
    while(i > 9)
    {
        i /= 10;
        ++ret;
    }
    return ret;
}

//print nemoProfile
void printLabelIdHash()
{
    std::map<graph64, map<int, int> >::iterator iter = nodeIdMap.begin();
    uint64 maxId = 0;    
    const int nodeIdWidth = 10;
    idOutfile.precision(5);
    idOutfile.flags( std::ofstream::right);

    //get the max nauty id width for printing
    for( ; iter != nodeIdMap.end(); iter++)
    {    
        graph64 id = iter->first;
        
        if(id > maxId)
        {
            maxId = id; 
        }
    }
        idOutfile << "_____________________________________________________________________________________________________________" << endl;

    int size = nodeIdMap.size();


    std::map<int, vector<int> >::iterator it = LabelNodeIdMap.begin();
    for(it = LabelNodeIdMap.begin() ; it != LabelNodeIdMap.end(); it++)
    {    
        int idIter = it->first;
        vector<int> myVec = it->second;
    
        for(int i = 0; i < size; ++i)
        {
           myVec.push_back(0);
        }
        LabelNodeIdMap[idIter]= myVec;
    }
    int vecIndex = 0;

    for(iter = nodeIdMap.begin() ; iter != nodeIdMap.end(); iter++)
    {   
        graph64 idIter = iter->first;
        labelNodeIdCountArr.push_back(idIter);
        map<int, int> countMap = iter->second;
        std::map<int, int>::iterator innerIter = countMap.begin();    
       
        //iter over nodeidmap innermap and populate labelNodeidmap with the label count for each node id
        for( ; innerIter != countMap.end(); innerIter++)
        {

            std::map< int, vector<int> >::iterator iter2 = LabelNodeIdMap.find(innerIter->first);
            std::vector< int >  myVec = iter2->second;
            myVec[vecIndex] = innerIter->second;
            iter2->second = myVec;
        }
        ++vecIndex;
    }
        std::map<int, vector<int> >::iterator it2 = LabelNodeIdMap.begin();
        idOutfile << setw(nodeIdWidth) << "Node Id:" << "  "; 
        idOutfile << setw(nodeIdWidth) << "ID:" << "  ";
        
        for(int i = 0; i < labelNodeIdCountArr.size(); i++)
        {
            idOutfile << labelNodeIdCountArr[i] << setw(nodeIdWidth) << "  " ;
        }
        idOutfile << endl;

        for( ; it2 != LabelNodeIdMap.end(); it2++)
        {
            vector< int >  myVec = it2->second;
            idOutfile << setw(nodeIdWidth) << it2->first << "  ";
            for(int j = 0; j < myVec.size(); j++)
            {
                idOutfile <<  setw(nodeIdWidth ) <<  myVec[j] << "     ";
            } 
            idOutfile << endl;
        }
        idOutfile << "_____________________________________________________________________________________________________________" << endl;

    idOutfile.close();

}

void addToLabelIdHash2(graph* canon, set* gv, const int G_N, const int G_M, vector<int> id )
{
    graph64 res_gr = getLabel(canon, gv, G_N, G_M);
//cout<< "resgraph" << res_gr <<endl;
    std::map<graph64, map<int, int> >::iterator iter = nodeIdMap.find( res_gr);
    //if key not found create vector and add it to map
    if(iter == nodeIdMap.end())
    {
        std::map<int, int> myMap;
        
        for(unsigned int i = 0; i < id.size(); i++)
        {
            myMap[id[i]]++;
        }
        nodeIdMap[res_gr] = myMap;  
    }
    else
    {
        std::map< int, int >  myMap = iter->second;
        for(unsigned int i = 0; i < id.size(); i++)
        {
            myMap[id[i]]++;
        }
        nodeIdMap[res_gr] = myMap;  
    }
}

/*
void addToLabelIdHash(graph* canon, set* gv, const int G_N, const int G_M, vector<int> id, int numNodes, int maxId)
{
    graph64 res_gr = getLabel(canon, gv, G_N, G_M);
//cout<< "resgraph" << res_gr <<endl;
    std::map<graph64, vector<int> >::iterator iter = nodeIdMap.find( res_gr);
    //if key not found create vector and add it to map
    if(iter == nodeIdMap.end())
    {
        std::vector< int > myVec;
        initNodeIds(myVec, maxId);

        for(int i = 0; i < id.size(); i++)
        {
            myVec[id[i]]++;
        }
        nodeIdMap[res_gr] = myVec;  
    }
    else
    {
        std::vector< int >  myVec = iter->second;
        for(int i = 0; i < id.size(); i++)
        {
            int node = id[i];
            myVec[node]++;
        }
        nodeIdMap[res_gr] = myVec;  
    }
}
*/
//prints nemoCounting data
void printLabelSubgraphs(PUNGraph Graph, int labelIter, vector<int> id, graph* canon, set* gv, const int G_N, const int G_M)
{
    graph64 res_gr = getLabel(canon, gv, G_N, G_M);
    
    if(id.size() != 0)
    {
        if(res_gr == labelId[labelIter])
        {
            randOutfile << "ID: " << labelId[labelIter] << endl;
            for(unsigned int i = 0; i < id.size(); i++)
            {
                randOutfile << id[i] << ", ";
            }
            randOutfile << endl;
            randOutfile << endl;
        }
    }
}

void addToLabelCountHash(graph* canon, set* gv, const int G_N, const int G_M, vector<int> id)
{

    graph64 result  = getLabel(canon, gv, G_N, G_M);
    LabelCountMap::iterator iter = labelMap.find( result);
    
    //if key not found create vector and add it to map
    if(iter == labelMap.end())
    {
        ResultCount vec; 
        initResultVec(vec);       
        vec[labelCountPtr]++;
        labelMap[result] = vec; 
        labelNodeIdVec.push_back(id);
 
    }
    else
    {
        ResultCount vec = iter->second;
        vec[labelCountPtr]++;
        labelMap[result] = vec;  
    }
}

//from fanmod function
double calcMeanFreq(TVec<TFlt>& vec, int networks)
{
    if(networks == 0)
        return sqrt(-1.0);

    double mean = 0;
    for(int i = 1; i <= networks; ++i)
    {
        mean += vec[i];
    }
    return mean/networks;
}

//from fanmod function
double calcStdDeviation(TVec<TFlt>& vec, int networks, double randMean)
{
    if(networks == 0)
        return sqrt(-1.0);
    double deviation = 0;
    for(int i = 1; i <= networks; i++)
    {
        deviation += powf((vec[i] - randMean), 2);
    }
    deviation /= (networks -1);
    return sqrt(deviation);
}

//prints significant subgraphs
void printLabelNodeHash()
{
    LabelNodeMap::iterator it;
       
    //iter through subgraphIds map
    for(it = subgraphIds.begin(); it != subgraphIds.end(); ++it)
    { 
        std::vector< std::vector<int> > outerVector = it->second;
        int outerSize = outerVector.size();
        *outfile << "Label: " << it->first << endl;
        //iter through outer vector
        for( int i=0; i<outerSize; ++i)
        {
            std::vector<int> innerVector = outerVector[i];
            *outfile << "{";
            int innerSize = innerVector.size();

            //iter through inner vector and print subgraph node ids
            for( int j = 0; j<innerSize; ++j)
            {
                *outfile << innerVector[j];
                if(j != (innerSize -1) )
                {
                    *outfile << ",";
                }
            }
            *outfile << "}" << endl;
        }
        *outfile <<  endl;                   
    }
    map < graph64, uint64 >::iterator iter;
}

void printLabelCountHashDirected()
{
/*
    int idx = 0;
    subgr_res gr;
    uint64 subgrCount = 0;//count for each label
    uint64 totalSubgrCount = 0;//total subgraph count for all labels in graph
    double subgraphs = 0.0;
    double totalSubgraphs = 0.0;
    LabelCountMap::iterator it;
    vector<subgr_res> resultVec(labelMap.size());
    
 
    //iter over labelMap 
    for(it = labelMap.begin(); it != labelMap.end(); ++it)
    { 
        ResultCount outerVector = it->second;
        gr.id = it->first; 
        subgrCount = outerVector[0];
        totalSubgrCount = countSubgr[0];
        subgraphs = (double) subgrCount;
        totalSubgraphs = (double) totalSubgrCount;

        gr.freq = (subgraphs / totalSubgraphs) ;
        concentration[0] = gr.freq;
        if(networks > 0)
        {
            gr.pValue = 0;
            gr.randMean = 0;
            gr.randSd = 0;

            for( int i=1; i< totalGraphs; ++i)
            {
                subgrCount = outerVector[i];
                totalSubgrCount = countSubgr[i];
                subgraphs = (double) subgrCount;
                totalSubgraphs = (double) randomCounter;
                if(concentration[i] > gr.freq)
                {
                    ++gr.pValue;
                }
            }

            gr.pValue /= double(networks);
            gr.randMean = calcMeanFreq(concentration, networks);                
            gr.randSd = calcStdDeviation(concentration, networks, gr.randMean);

            if(gr.randSd == 0)
            {
                gr.zScore = sqrt(-1);
            }
            else
            {
                gr.zScore = (gr.freq - gr.randMean) / gr.randSd;
            }
        }
        resultVec[idx++] = gr;
    }
*/
}



//undirected graphs only
void printLabelCountHash(const PUNGraph& Graph, const int G_N)
{
    int idx = 0;
    subgr_res gr;
    uint64 subgrCount = 0;//count for each label
    uint64 totalSubgrCount = 0;//total subgraph count for all labels in graph
    double subgraphs = 0.0;
    double totalSubgraphs = 0.0;
    LabelCountMap::iterator it;
    vector<subgr_res> resultVec(labelMap.size());
    int freqW = 10;
    int mFreqW = 13;
    int sdWidth = 14;
    int zWidth = 11;
    int pWidth = 9;
    uint64 maxId = 0;
    outfile->precision(5);
    outfile->flags( std::ofstream::right);
    //iter over labelMap 
    for(it = labelMap.begin(); it != labelMap.end(); ++it)
    { 
        ResultCount outerVector = it->second;
        gr.id = it->first;
        if(gr.id > maxId)
        {
            maxId = gr.id; 
        }
        subgrCount = outerVector[0];
    // cerr << "subgrCount " << subgrCount << endl;
        totalSubgrCount = countSubgr[0];
//cerr<< "totalSubgr " << totalSubgrCount << endl;
        subgraphs = (double) subgrCount;
//cerr << "subgrcount dbl " << subgraphs << endl;
        totalSubgraphs = (double) totalSubgrCount;
//cerr << "totalSubgr dbl " << totalSubgraphs << endl;
        gr.freq = (subgraphs / totalSubgraphs) ;
        concentration[0] = gr.freq;
//cerr << "________________" << endl << endl;
 
        if(networks > 0)
        {
            gr.pValue = 0;
            gr.randMean = 0;
            gr.randSd = 0;

            for( int i=1; i< totalGraphs; i++)
            {
                subgrCount = outerVector[i];
                totalSubgrCount = countSubgr[i];
                subgraphs = (double) subgrCount;
                totalSubgraphs = (double) totalSubgrCount;

//cerr << "con: " << concentration[i] << endl;
                if(subgraphs == 0.0 && totalSubgraphs == 0.0)
                {
                    concentration[i] = 0; 
                }
                else
                {
                    concentration[i] = (subgraphs / totalSubgraphs);
                }
 
                if(concentration[i] >= gr.freq)
                {
                    ++gr.pValue;
                }
            }
            gr.pValue /= double(networks);
            gr.randMean = calcMeanFreq(concentration, networks);                
            gr.randSd = calcStdDeviation(concentration, networks, gr.randMean);
//cerr << "mean: " << gr.randMean << endl;
//cerr << "sd: " << gr.randSd << endl;
            if(gr.randSd == 0)
            {
                gr.zScore = sqrt(-1);
            }
            else
            {
                gr.zScore = (gr.freq - gr.randMean) / gr.randSd;
            }
        }
        resultVec[idx++] = gr;
    }
    const int idWidth = numDigits(maxId);
    //Print results to file
    //labelNodeIdVec
    *outfile << "__________________________________________________________________" << endl;
    *outfile << setw(idWidth ) << "ID" << ' ' 
             << setw(freqW + 2) << "Frequency"; 

    if(networks > 0)
    {
        *outfile << setw(mFreqW + 1) <<  "Mean-Freq" << setw(sdWidth) << "Standard-Dev"
                 << setw(zWidth) << "Z-Score" << setw(pWidth) << "p-Value";
    }
        
    *outfile << endl;
    *outfile << setw(idWidth) << ' '  << setw(mFreqW ) << " [Original]";

    if(networks > 0)
    {
        *outfile << setw(mFreqW + 1) << "[Random]" << setw(sdWidth) << "[Random]";
    }
    *outfile << endl << endl;
            
    for(unsigned int i = 0; i < resultVec.size(); i++)
    {
        //datalines of the result table
        *outfile << setw(idWidth) << resultVec[i].id << ' ';
        *outfile << setw(freqW) << resultVec[i].freq * 100 << '%';
        
        if(networks > 0)
        {
            *outfile << setw(mFreqW) << resultVec[i].randMean * 100 << '%'
                     << setw(sdWidth) << resultVec[i].randSd
                     << setw(zWidth) << resultVec[i].zScore 
                     << setw(pWidth) << resultVec[i].pValue;
        }
        
        *outfile <<  endl << endl;
        
        if(hashType == "subgraph")
        {
            if(resultVec[i].pValue < 0.01 && networks >= 1000 )
            {
                labelId.push_back(resultVec[i].id);
            }
            if(resultVec[i].zScore > 2.0 && networks < 1000)
            {
                labelId.push_back(resultVec[i].id);
            }
        }
        gettimeofday(&start2,NULL);   
        if(s_graphviz == "y")
        {
            TStr myString;
            //print label using graphviz
            if(labelId.size() != 0)
            {
                for(int i = 0; i < labelId.size(); i++)
                {
                    int id = int(labelId[i]);
                    // char* fileExt = ".png";
                    char strs[256] ;
                    int base = sprintf(strs, "%d", id );
                    char* extension = ".png";
                    myString = TStr(strs);
                    strcat(strs, extension);
                    TVec<TInt> vec = convertIntVec(labelNodeIdVec[i]);
                    PUNGraph subgraph = TSnap::GetSubGraph(Graph, vec);
                    TGVizLayout layout = (TGVizLayout)0;
                    bool label = false;
                    TStr fileName = TStr(strs);
                    //TStr fileName = "graphImages.png";
                    TIntStrH nidColor;
        
                    const TStr color = "white";
                    for(int i = 0; i < vec.Len(); i++)
                    {
                        nidColor.AddKey(i);
                        nidColor[i] = color;
                    }
                    TSnap::DrawGViz( subgraph, layout, fileName, myString, label, nidColor);
                }   
            }
        }
        gettimeofday(&end2,NULL);   
        long seconds2  = end2.tv_sec  - start2.tv_sec;
        long useconds2 = end2.tv_usec - start2.tv_usec;
        mtimeGraphviz = ((seconds2 * 1000)  + useconds2/1000.0) ;
    }
        *outfile << "__________________________________________________________________" << endl;
}

void initCountSubg()
{
    for(int i = 0; i < totalGraphs; i++)
    {
        countSubgr.Add(0);
    }
}

void initConcentration()
{
    for(int i = 0; i < totalGraphs; i++)
    {
        concentration.Add(0);
    }
}

bool isEdgeToNodes( const PUNGraph& graph, const TVec<TInt>& subgraph, int node)
{
  for( int i=0; i< subgraph.Len() ; ++i)
  {
    if(graph->IsEdge(subgraph[i], node))
      return true;
  }
  return false;
}

//////BEGIN UNDIRECTED FUNCTIONS///////////////////////////////////////////
//finds the target node's edges in Undirected graph
void getAdjacenciesOfNodesInSubgraphUndirected(const PUNGraph& graph, 
                                     const TVec<TInt>& subgraph, 
                                     const TVec<TInt>& Extension,
                                     TVec<TInt>& currNodeAdj,
                                     int targetNode, int G_N,  int currNode)
{
//cerr << "SUBGRAPH: " ;
//printVector(subgraph);
//cerr << endl;
    const TUNGraph::TNodeI NI = graph->GetNI(currNode);
    int deg = NI.GetOutDeg();

    //iter over edges 
    for(int j = 0; j < deg; j++)
    {
        int outNodeId = NI.GetOutNId(j);//current edge 

        if(targetNode < outNodeId
            //not an edge to subgraph 
           //&& ! isEdgeToNodes( graph, subgraph, currNode )
           
           && ! TVecContains(currNodeAdj, outNodeId)
           && ! TVecContains(Extension, outNodeId)
           && ! TVecContains(subgraph, outNodeId) )
        {
            currNodeAdj.Add(outNodeId);
        }
    }


    if(randGraph)
    {
        TVec<TInt> combined = Extension;
        for(int i = 0; i < currNodeAdj.Len(); ++i)
        {
          combined.Add(currNodeAdj[i]);
        }

        double randNum =  prob[probIter];
        double percent = randNum/100; 
        int randSubgraphEnum2 =  percent * combined.Len() + 0.5;
        while(combined.Len() > randSubgraphEnum2)
        {
            for(int i = 0; i < combined.Len(); i++)
            { 
                if(combined.Len() > randSubgraphEnum2) 
                {

                    int digit = rand() % 2;
                    if(digit)
                    {
                        combined.Del(i);
                    }
                } 
            }
        }
    }
}

//find nodes call addelement on for each graphrow call
void editNautyGraphUndirected(int graphSet, const PUNGraph& Graph,
                    TVec<TInt> idSubg, 
                    const int G_M, 
                    set* gv, 
                    const int G_N,
                    graph* nautyGraph, 
                    int numNodes, 
                    int* lab, 
                    int* ptn, 
                    set* theSet, 
                    int* orbits, 
                    optionblk& options,
	                statsblk& stats, 
                    setword* workspace, 
                    int maxM, 
                    graph* canon)
{
//printVector(idSubg);
    PUNGraph subgraph = TSnap::GetSubGraph(Graph, idSubg);

     //call ADDELEMENT for each edge 
    for(int i = 0; i < G_N; i++)
    {
        for(int j = 0; j < G_N; j++)
        {
            if(subgraph->IsEdge(idSubg[i], idSubg[j]))
            {
                //NAUTY calls
                DELELEMENT(GRAPHROW(nautyGraph, i, G_M), j);
                gv = GRAPHROW(nautyGraph, i, G_M); 
                ADDELEMENT(gv, j);

            }
        }
    }
}

#if 0
//for Undirected graphs:for each node in graph adds qualifying adjacencies to the vectors Extension and VSubgraph
//base case: VSubgraph length =size k
void extendSubgraphUndirected_backup(const PUNGraph& Graph, 
                                TVec<TInt>& VSubgraph, 
                                TVec<TInt>& Extension, 
                                set* gv, 
                                const int G_M,
                                const int G_N, 
                                graph* nautyGraph, 
                                int numNodes,
                                int* lab, 
                                int* ptn, 
                                set* theSet, 
                                int* orbits, 
                                optionblk& options,
                                statsblk& stats, 
                                setword* workspace, 
                                int maxM, 
                                graph* canon, int targetNode)
{
        //printVector(  VSubgraph);
    TVec<TInt> myExtension = Extension;
    TVec<TInt> mySubgraph = VSubgraph;
    vector<int> id;

    //if a subgraph is found add to id vector for labelNodeIdMap
    if(VSubgraph.Len() ==  G_N)
    {
       for(int i = 0; i < VSubgraph.Len(); i++)
       {
           id.push_back(int(VSubgraph[i]) );   
       }
     //for testing
//printVector(mySubgraph);


        //for each node in subgraph that corresponds to graphrow sets edge data for nauty
        for(int i = 0; i < G_N; i++)
        {
            editNautyGraphUndirected(i, Graph, VSubgraph, G_M, gv, G_N, 
                                    nautyGraph, numNodes, 
                                    lab,  ptn, NILSET,  orbits, options,
                                    stats, workspace, 160 * MAXM,  canon); 
        }
        //NAUTY call for canonical labelling
        nauty(nautyGraph, lab, ptn, NILSET, orbits, &options, &stats, workspace, 160 * MAXM, G_M, G_N, canon);
        if(original)
        { 
            addToLabelCountHash(canon, gv, G_N, G_M, id);
            countSubgr[countSubgrPtr]++;//TODO could this be the problem??
        }
 
        if(hashType == "subgraph" && !randGraph)
        {       
            int maxNId = Graph->GetMxNId(); 
            addToLabelIdHash(canon, gv, G_N, G_M, id, numNodes, maxNId);
        }

        if(hashType == "none")
        {
            printLabelSubgraphs(Graph, labelIter,  id, canon, gv, G_N, G_M);
        }

        //after each subgraph clear the set data for each graphrow
        for (int i = 0; i != G_N; ++i) 
        {
            EMPTYSET(GRAPHROW(nautyGraph, i, G_M), G_M);
        }

        if(!randGraph)
        {
            subgraphCounter++;
        }
        else
        {
            randomCounter++;
        }

           // mySubgraph.Clr(true, -1);
       // mySubgraph.DelLast();
        return;
    }         
    

    while(!myExtension.Empty())
    {
        int currNode = 0;
        currNode = myExtension[0];
        mySubgraph.Add(currNode);
        myExtension.Del(0);
        
        if(randGraph)
        {
            if(VSubgraph.Len() != G_N)
            {
                 probIter = VSubgraph.Len();
            }
        }
        getAdjacenciesOfNodesInSubgraphUndirected(Graph, mySubgraph, myExtension, targetNode, G_N, currNode);
        
    extendSubgraphUndirected(Graph,
                                mySubgraph, 
                                myExtension, 
                                gv, 
                                G_M,
                                G_N, 
                                nautyGraph,                  
                                numNodes,
                                lab, 
                                ptn, 
                                gv, 
                                orbits, 
                                options, 
                                stats, 
                                workspace, 
                                maxM, 
                                canon, targetNode);
    }
    mySubgraph.DelLast();

    if(mySubgraph.Len() != G_N)
    {
        probIter = VSubgraph.Len();
    }
}
#endif

void setNautyGraph(vector<int> id, TVec<TInt> idSubg, const PUNGraph& Graph, 
                                set* gv, 
                                const int G_M,
                                const int G_N, 
                                graph* nautyGraph, 
                                int numNodes,
                                int* lab, 
                                int* ptn, 
                                set* theSet, 
                                int* orbits, 
                                optionblk& options,
                                statsblk& stats, 
                                setword* workspace, 
                                int maxM, 
                                graph* canon ) 
{
    for(int i = 0; i < G_N; i++)
    {
        editNautyGraphUndirected(i, Graph, idSubg, G_M, gv, G_N, 
                                    nautyGraph, numNodes, 
                                    lab,  ptn, NILSET,  orbits, options,
                                    stats, workspace, 160 * MAXM,  canon); 
   }

    //printVector(idSubg);
    nauty(nautyGraph, lab, ptn, NILSET, orbits, &options, &stats, workspace, 160 * MAXM, G_M, G_N, canon); 
    
    countSubgr[countSubgrPtr]++;
    
        addToLabelCountHash(canon, gv, G_N, G_M, id);
    //if(original)
    //{
      //  addToLabelCountHash(canon, gv, G_N, G_M, id);
   // }

    if(hashType == "subgraph" && !randGraph)
    {       
        addToLabelIdHash2(canon, gv, G_N, G_M, id);
    }
    if(hashType == "none")
    {
        printLabelSubgraphs(Graph, labelIter,  id, canon, gv, G_N, G_M);
    }

    //after each subgraph clear the set data for each graphrow
    for (int i = 0; i != G_N; ++i) 
    {
        EMPTYSET(GRAPHROW(nautyGraph, i, G_M), G_M);
    }
}

void printEnumeratedSubgraphs( TVec<TInt>& subg, TVec<TInt>& ext, const PUNGraph& Graph, 
                                set* gv, 
                                const int G_M,
                                const int G_N, 
                                graph* nautyGraph, 
                                int numNodes,
                                int* lab, 
                                int* ptn, 
                                set* theSet, 
                                int* orbits, 
                                optionblk& options,
                                statsblk& stats, 
                                setword* workspace, 
                                int maxM, 
                                graph* canon) 
{
    int numExtensions = ext.Len();
    TVec<TInt> idSubg;

    for (int i=0; i<numExtensions; ++i)
    {
        vector<int> id;
        subg.Add(ext[i]);
//        randOutfile << "[" ;

        for(int i = 0; i < subg.Len(); i++)
        {
            id.push_back(subg[i] );  
            idSubg.Add(subg[i]);
        }
        setNautyGraph(id, idSubg, Graph, gv, G_M, G_N, 
                                    nautyGraph, numNodes, 
                                    lab,  ptn, NILSET,  orbits, options,
                                    stats, workspace, 160 * MAXM,  canon);


  //      randOutfile << "]  " << endl;
        idSubg.Clr(true, -1); 
        subg.DelLast();

        if(original)
        {
            subgraphCounter++;
        }
        else
        {
            randomCounter++;
        }
    }
}


void getAdjacenciesOfNodesInSubgraphUndirected2(const PUNGraph& graph,
                                                const TVec<TInt>& accumulated, 
                                                const TVec<TInt>& subgraph, 
                                                const TVec<TInt>& Extension,
                                                TVec<TInt>& currNodeAdj,
                                                int targetNode, 
                                                int G_N,  int currNode)
{
  const TUNGraph::TNodeI NI = graph->GetNI(currNode);

  int deg = NI.GetOutDeg();
  for(int j = 0; j < deg; j++)
  {
    int outNodeId = NI.GetOutNId(j);//current edge 

    if(targetNode < outNodeId)
    {
      for( int k=0; k< subgraph.Len(); k++)
      {
        if( ! graph->IsEdge(subgraph[k], outNodeId))
        {
           if( !TVecContains(subgraph, outNodeId) &&
               !TVecContains(Extension, outNodeId) && 
               !TVecContains(currNodeAdj, outNodeId) &&
               !TVecContains(accumulated, outNodeId))       
           {

             currNodeAdj.Add(outNodeId);
           }
        }
      }
    }
  }
}

static int c1 = 0;
//for Undirected graphs:for each node in graph adds qualifying adjacencies to the vectors Extension and VSubgraph
//base case: VSubgraph length =size k
void extendSubgraphUndirected(const PUNGraph& Graph, 
                                TVec<TInt> myAccumulated,
                                TVec<TInt>& VSubgraph, 
                                TVec<TInt>& Extension, 
                                set* gv, 
                                const int G_M,
                                const int G_N, 
                                graph* nautyGraph, 
                                int numNodes,
                                int* lab, 
                                int* ptn, 
                                set* theSet, 
                                int* orbits, 
                                optionblk& options,
                                statsblk& stats, 
                                setword* workspace, 
                                int maxM, 
                                graph* canon, int targetNode)
{
    c1 += 2;
   // for( int i=0; i<c1; ++i) cerr << " ";
   // cerr << "rec";
   // cerr << " SUB:"; printVector(VSubgraph);
    //cerr << " EXT:"; printVector(Extension);     

    TVec<TInt> myExtension = Extension;
    TVec<TInt> mySubgraph = VSubgraph;

    if(VSubgraph.Len() ==  G_N -1)
    {
        printEnumeratedSubgraphs( mySubgraph,  myExtension, Graph, 
                                 gv, 
                                 G_M,
                                 G_N, 
                                 nautyGraph, 
                                 numNodes,
                                 lab, 
                                 ptn, 
                                 theSet, 
                                 orbits, 
                                 options,
                                 stats, 
                                 workspace, 
                                 maxM, 
                                 canon); 
   // cerr << "FOUND: "; printVector(mySubgraph);
      //  cerr << endl;
        c1 -=2;
        return;
    } 
    //cerr << endl;        
   

    TVec<TInt> accumulated = myAccumulated;
    while(!myExtension.Empty())
    {
//static int blah = 0;
//if(blah++ > 10) return;    
     // for( int i=0; i<c2; ++i) cerr << " ";
     // cerr << "while" << endl;     
      int currNode = myExtension[0];
      mySubgraph.Add(currNode); 
      accumulated.Add(currNode);
      myExtension.Del(0);
       
      TVec<TInt> currNodeAdj; 
      getAdjacenciesOfNodesInSubgraphUndirected2(Graph, accumulated, mySubgraph, 
                                                 myExtension,  currNodeAdj, 
                                                 targetNode, G_N, currNode);
      
        TVec<TInt> combinedExt = myExtension;
//cerr<< "HERE: "; printVector(currNodeAdj); cerr << endl;
        for(int i = 0; i < currNodeAdj.Len(); ++i)
        {
          combinedExt.Add(currNodeAdj[i]);
        }
      //  combinedExt.Sort();
   
        TVec<TInt> combinedSubg = mySubgraph;
        extendSubgraphUndirected(Graph,
                                accumulated,
                                combinedSubg, 
                                combinedExt, 
                                gv, 
                                G_M,
                                G_N, 
                                nautyGraph,                  
                                numNodes,
                                lab, 
                                ptn, 
                                gv, 
                                orbits, 
                                options, 
                                stats, 
                                workspace, 
                                maxM, 
                                canon, targetNode);
        mySubgraph.DelLast();
    }
    c1 -=2;
}


void enumerateSubgraphsRand(const PUNGraph& Graph,
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{  
    TVec<TInt> VSubgraph;
    TVec<TInt> VExtension;
    probIter = 0;

    double randNum =  prob[probIter];
    double percent = randNum/100; 
    int randSubgraphEnum = percent * Graph->GetNodes() + 0.5;

    randNum =  prob[probIter];
    percent = randNum/100; 
    randSubgraphEnum = percent * Graph->GetNodes() + 0.5;

    TVec<TInt> nodesChosen;
    int count = 0; 

    probIter++;
    while(count < randSubgraphEnum)
    {
                
        for(int i = 0; i < randomIds.Len(); i++)
        { 
            if(count < randSubgraphEnum)  
            {
                int digit = rand() % 2;
                if(digit)
                {
                    if(!nodesChosen.Empty())
                    {
                        for(int j = 0; j < nodesChosen.Len(); j++)
                        {
                            if(randomIds[i] == nodesChosen[j])
                            {
                                i++;
                                continue;
                            }
                        }
                    }
                    VSubgraph.Clr(true, -1);
                    VExtension.Clr(true, -1);
                    TSnap::DelSelfEdges(Graph);
                    const TUNGraph::TNodeI NI = Graph->GetNI(randomIds[i]);
                    int targetNode = NI.GetId();
                    VSubgraph.Add(targetNode);
                    nodesChosen.Add(targetNode);  
  
                    //iterate thru the targetNode's edges
                    for (int e = 0; e < NI.GetOutDeg(); e++)
                    {
                        if(NI.GetId() < NI.GetOutNId(e))
                        {
                            VExtension.Add(NI.GetOutNId(e)); 
                        }
                    }
                            
                    randNum =  prob[probIter];
                    percent = randNum/100; 
                    int randSubgraphEnum2 =  percent * VExtension.Len() + 0.5;
                            
                     while(VExtension.Len() > randSubgraphEnum2)
                     {
                
                            for(int i = 0; i < VExtension.Len(); i++)
                            { 
                               if(VExtension.Len() > randSubgraphEnum2) 
                               {       

                                   int digit = rand() % 2;
                                   if(digit)
                                   {   
                                        VExtension.Del(i);
                                   }   
                               } 
                           }
                       }
                       TVec<TInt> accumulated = VSubgraph;
                       extendSubgraphUndirected(Graph, accumulated, VSubgraph, VExtension,   
                                                gv, 
                                                G_M, 
                                                G_N, 
                                                nautyGraph, 
                                                numNodes, 
                                                lab, 
                                                ptn, 
                                                NILSET, 
                                                orbits, 
                                                options,
                                                stats, 
                                                workspace, 
                                                160 * MAXM, canon, targetNode);

                    count++;
                }
            }
        }
    }
}

//for Undirected graphs: adds qualifying nodes to the vectors Extension and VSubgraph
//base case: VSubgraph length =size k
void enumerateSubgraphsUndirected(const PUNGraph& Graph,
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{  
    TVec<TInt> VSubgraph;
    TVec<TInt> VExtension;
    probIter = 0;
    double timeCalc = 0;
    int print = 0;

    for (TUNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) 
    {
        callCount++;
        timeCalc = (numNodes * .10);
        int total = timeCalc + 0.5;
        print = total;

        //if original graph print progress update to stdout
        if(original)
        {    
            if(total > 0)
            {
                std::vector<int> myVec;
               // myVec.push_back(0);
                LabelNodeIdMap[NI.GetId()] = myVec;
                //nodeIds.push_back(NI.GetId());
                
                if(callCount %  total == 0)
                {
                    print += total;
                    cerr<< "Original Graph Total Progress: " << callCount << "/" << numNodes << endl;
                }
            }

            if(callCount == numNodes)
            {
                cerr<< "Original Graph Total Progress Complete "  << endl;
            }
        }
        VSubgraph.Clr(true, -1);
        VExtension.Clr(true, -1);
        TSnap::DelSelfEdges(Graph);
        int targetNode = NI.GetId();
       //int targetNode = nodes[i];
        VSubgraph.Add(targetNode);
        const TUNGraph::TNodeI NI = Graph->GetNI(targetNode);
       //iterate thru the targetNode's edges
        for (int e = 0; e < NI.GetOutDeg(); e++)
        {
            if(NI.GetId() < NI.GetOutNId(e))
            {
                VExtension.Add(NI.GetOutNId(e)); 
            }
        }
        TVec<TInt> accumulated = VSubgraph;
        extendSubgraphUndirected(Graph, accumulated, VSubgraph, VExtension,   
                                gv, 
                                G_M, 
                                G_N, 
                                nautyGraph, 
                                numNodes, 
                                lab, 
                                ptn, 
                                NILSET, 
                                orbits, 
                                options,
                                stats, 
                                workspace, 
                                160 * MAXM, canon, targetNode);  
    }
}
//for Undirected graphs: adds qualifying nodes to the vectors Extension and VSubgraph
//base case: VSubgraph length =size k
void enumerateSubgraphsUndirected_backup(const PUNGraph& Graph,
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{  
    TVec<TInt> VSubgraph;
    TVec<TInt> VExtension;
    probIter = 0;

    double randNum =  prob[probIter];
    double percent = randNum/100; 
    int randSubgraphEnum = percent * Graph->GetNodes() + 0.5;
    double timeCalc = 0;
    int print = 0;

    //if ORIGINAL graph or hashType = subgraphs
    if(!randGraph)
    {
        for (TUNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) 
        {
            callCount++;
            timeCalc = (numNodes * .10);
            int total = timeCalc + 0.5;
            print = total;
            
           cerr <<  "  origninal: " << original << endl; 
            if(original && hashType != "none")
            {
                if(total > 0)
                {
                    if(callCount %  total == 0)
                    {
                        print += total;
                        cerr<< "Original Graph Total Progress: " << callCount << "/" << numNodes << endl;
                    }
                }

                if(callCount == numNodes)
                {
                    cerr<< "Original Graph Total Progress Complete "  << endl;
                }
            }

            VSubgraph.Clr(true, -1);
            VExtension.Clr(true, -1);
            TSnap::DelSelfEdges(Graph);
            int targetNode = NI.GetId();
            VSubgraph.Add(targetNode);
            //iterate thru the targetNode's edges
            for (int e = 0; e < NI.GetOutDeg(); e++)
            {
                if(NI.GetId() < NI.GetOutNId(e))
                {
                    VExtension.Add(NI.GetOutNId(e)); 
                }
            }
           // printVector(VExtension);//TODO
            TVec<TInt> accumulated = VSubgraph;
            extendSubgraphUndirected(Graph, accumulated, VSubgraph, VExtension,   
                                gv, 
                                G_M, 
                                G_N, 
                                nautyGraph, 
                                numNodes, 
                                lab, 
                                ptn, 
                                NILSET, 
                                orbits, 
                                options,
                                stats, 
                                workspace, 
                                160 * MAXM, canon, targetNode);  
        }
    }   
    randNum =  prob[probIter];
    percent = randNum/100; 
    randSubgraphEnum = percent * Graph->GetNodes() + 0.5;

    TVec<TInt> nodesChosen;
    int count = 0; 

    if(randGraph)
    {
        probIter++;
        while(count < randSubgraphEnum)
        {
                
            for(int i = 0; i < randomIds.Len(); i++)
            { 
                if(count < randSubgraphEnum)  
                {
                    int digit = rand() % 2;
                    if(digit)
                    {
                        if(!nodesChosen.Empty())
                        {
                            for(int j = 0; j < nodesChosen.Len(); j++)
                            {
                                if(randomIds[i] == nodesChosen[j])
                                {
                                    i++;
                                    continue;
                                }
                            }
                        }
                            VSubgraph.Clr(true, -1);
                            VExtension.Clr(true, -1);
                            TSnap::DelSelfEdges(Graph);
                            const TUNGraph::TNodeI NI = Graph->GetNI(randomIds[i]);
                            int targetNode = NI.GetId();
                            VSubgraph.Add(targetNode);
                            nodesChosen.Add(targetNode);  
  
                            //iterate thru the targetNode's edges
                            for (int e = 0; e < NI.GetOutDeg(); e++)
                            {
                                if(NI.GetId() < NI.GetOutNId(e))
                                {
                                    VExtension.Add(NI.GetOutNId(e)); 
                                }
                            }
                            
                            randNum =  prob[probIter];
                            percent = randNum/100; 
                            int randSubgraphEnum2 =  percent * VExtension.Len() + 0.5;
                            while(VExtension.Len() > randSubgraphEnum2)
                            {
                
                                for(int i = 0; i < VExtension.Len(); i++)
                                { 
                                    if(VExtension.Len() > randSubgraphEnum2) 
                                    {       

                                        int digit = rand() % 2;
                                        if(digit)
                                        {   
                                            VExtension.Del(i);
                                        }   
                                    } 
                                }
                            }
                            TVec<TInt> accumulated = VSubgraph;
                            extendSubgraphUndirected(Graph, accumulated, VSubgraph, VExtension,   
                                                gv, 
                                                G_M, 
                                                G_N, 
                                                nautyGraph, 
                                                numNodes, 
                                                lab, 
                                                ptn, 
                                                NILSET, 
                                                orbits, 
                                                options,
                                                stats, 
                                                workspace, 
                                                160 * MAXM, canon, targetNode);

                        count++;
                    }
                }
            }
        }
    }
}


void randomUndirected(PUNGraph& Graph,  
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{
    long progress = 0;
    int print = 0;
    //double total = 0;
    long mtime = 0;
 
    while(networkCount < networks)
    {
        countSubgrPtr++;
        labelCountPtr++;

        gettimeofday(&start1, NULL);
        TIntV resultDegree;
        //get orignal graph deg sequence vector
        TSnap::GetDegSeqV(Graph, resultDegree);
        TRnd rand = (time(NULL), 0);
        PUNGraph graph = dirMethod(resultDegree, rand);
        TSnap::DelSelfEdges(graph);
        gettimeofday(&end1, NULL);
        long seconds  = end1.tv_sec  - start1.tv_sec;
        long useconds = end1.tv_usec - start1.tv_usec;
        mtime += ((seconds *1000 ) + (useconds/1000.0));

        for (TUNGraph::TNodeI NI = graph->BegNI(); NI < graph->EndNI(); NI++) 
        {
            randomIds.Add(NI.GetId());    
        }

        enumerateSubgraphsRand(graph, 
                                         gv, 
                                         G_M, 
                                         G_N, 
                                         nautyGraph, 
                                         numNodes, 
                                         lab, 
                                         ptn,
                                         NILSET, 
                                         orbits, 
                                         options,
                                         stats, 
                                         workspace, 
                                         160 * MAXM, 
                                         canon);
        networkCount++;
        randCount++;
        progress = (networks * .10);
        int total = progress + 0.5;
        print = total;
            
        if(total > 0)
        {
            if(networkCount %  total == 0)
            {
                print += total;
                cerr<< "Random Graph Total Progress: " << networkCount << "/" << networks << endl;
            }
        }

        if(networkCount == networks)
        {
            cerr<< "Random Graph Total Progress Complete "  << endl;
        }
    }
    //total = double(mtime) / double (1000);
    randTime = double(mtime);
    //timeDiff = total;
}

//nemoCounting: generate label subgraphs for signicant motifs
void generateLabelSubgraphs(PUNGraph& Graph,  
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM,
                                    graph* canon)
{
    TVec<TInt> nodeList;

/*
    if(s_nemoCount != 0)
    {
//TODO add error handling for bad input to neMoCount
        std::map<graph64, map<int, int> >::iterator iter = nodeIdMap.find( s_nemoCount);
        std::map<int, int> myMap = iter->second;
        std::map<int, int>::iterator innerIter = myMap.begin();
 
        for( ; innerIter != myMap.end(); innerIter++)
        {
            nodeList.Add(innerIter->first);
        }

        PUNGraph graph = TSnap::GetSubGraph(Graph, nodeList);
        nodeList.Clr();
        enumerateSubgraphsUndirected(graph, 
                                         gv, 
                                         G_M, 
                                         G_N, 
                                         nautyGraph, 
                                         numNodes, 
                                         lab, 
                                         ptn,
                                         NILSET, 
                                         orbits, 
                                         options,
                                         stats, 
                                         workspace, 
                                         160 * MAXM, 
                                         canon);
    }
    else
*/
    //if(s_nemoCount == "y")
    //{
        //
        for(unsigned int i = 0; i < labelId.size(); i++)
        {
            std::map<graph64, map<int, int> >::iterator iter = nodeIdMap.find( labelId[i]);
            std::map<int, int> myMap = iter->second;
            std::map<int, int>::iterator innerIter = myMap.begin();
 
            for( ; innerIter != myMap.end(); innerIter++)
            {
                nodeList.Add(innerIter->first);
            }

            PUNGraph graph = TSnap::GetSubGraph(Graph, nodeList);
            nodeList.Clr();
            enumerateSubgraphsUndirected(graph, 
                                         gv, 
                                         G_M, 
                                         G_N, 
                                         nautyGraph, 
                                         numNodes, 
                                         lab, 
                                         ptn,
                                         NILSET, 
                                         orbits, 
                                         options,
                                         stats, 
                                         workspace, 
                                         160 * MAXM, 
                                         canon);
            labelIter++;
        }
        randOutfile.close();
   // }
} 
//////END UNDIRECTED FUNCTIONS///////////////////////////////////////////



//BEGIN DIRECTED FUNCTIONS///////////////////////////////////////////
//called for each node in the subgraph to find it's edges within VSubgraph
void setEdgesDirected(int targetNode, const PNGraph& Graph,  TVec<TInt>& VSubgraph, set* gv, const int G_M, graph* nautyGraph)
{

    PNGraph subgraph = TSnap::GetSubGraph(Graph, VSubgraph);//make subgraph from VSubgraph
    //TVec<TInt> edges;

    //for(int i = 0; i < VSubgraph.Len(); i++)
    //{         
        //edges.Add(subgraph->IsEdge(VSubgraph[targetNode], VSubgraph[i]));

    //}

        //call ADDELEMENT for each edge 
    for(int i = 0; i < VSubgraph.Len(); i++)
    {

        if(subgraph->IsEdge(VSubgraph[targetNode], VSubgraph[i]))
        {      

            //NAUTY calls
            DELELEMENT(GRAPHROW(nautyGraph, targetNode, G_M), i);
            gv = GRAPHROW(nautyGraph, targetNode, G_M); 
            ADDELEMENT(gv, i);//call on ith element to set(targetNode) bit = 1 in gv
        }
    }
    //edges.Clr();        
}


//find nodes call addelement on for each graphrow call
void editNautyGraphDirected(int graphSet, const PNGraph& Graph,
                    TVec<TInt>& VSubgraph, 
                    const int G_M, 
                    set* gv, 
                    const int G_N,
                    graph* nautyGraph, 
                    int numNodes, 
                    int* lab, 
                    int* ptn, 
                    set* theSet, 
                    int* orbits, 
                    optionblk& options,
	                statsblk& stats, 
                    setword* workspace, 
                    int maxM, 
                    graph* canon)
{

    setEdgesDirected(graphSet, Graph, VSubgraph, gv, G_M, nautyGraph);
}


//finds the target node's edges in Directed graph
void GetAdjacenciesOfNodesInSubgraphDirected(const PNGraph& graph, 
                                     const TVec<TInt>& subgraph, 
                                     TVec<TInt>& extension,
                                     int targetNode, int currNode, const TVec<TInt>& VExtension)
{  

    int isValid = true;

    for(int i = 0; i < VExtension.Len(); i++)
    {
        extension.Add(VExtension[i]);
    }

    //for (TNGraph::TNodeI NI = graph->BegNI(); NI < graph->EndNI(); NI++) 
    //{
    TSnap::DelSelfEdges(graph);

    const TNGraph::TNodeI NI = graph->GetNI(currNode);
    int outDeg = NI.GetOutDeg();
    int inDeg = NI.GetInDeg();
    int id = 0;


    for(int i = 0; i < outDeg; i++)
    {
            
        id = NI.GetOutNId(i);
        isValid = true;
        if(id > targetNode)
        {

            if( ! TVecContains(subgraph, id) &&
                ! TVecContains(extension, id ) )
            {
                for(int j = 0; j < subgraph.Len(); j++)
                {        
                    if(currNode != subgraph[j])
                    {      
                        int sub = subgraph[j];
                        if(graph->IsEdge(sub, id ) || graph->IsEdge(id, sub ) )
                        {
                            isValid = false;
                        } 
                     }                                                        
                 }  
                
                 if(isValid)
                 {
                    extension.Add(id);  
                           
                 }
            }
        }
    }


    for(int i = 0; i < inDeg; i++)
    {
            
        id = NI.GetInNId(i);
        isValid = true;
        if(id > targetNode)
        {

            if( ! TVecContains(subgraph, id) &&
                ! TVecContains(extension, id ) )
            {

                for(int j = 0; j < subgraph.Len(); j++)
                {        
                    if(currNode != subgraph[j])
                    {      
                        int sub = subgraph[j];
                        if(graph->IsEdge(sub, id ) || graph->IsEdge(id, sub ) )
                        {
                            isValid = false;
                        } 
                     }                                                        
                 }  
                
                 if(isValid)
                 {

                    extension.Add(id);  
                           
                 }
            }
        }
    }

}


//for Directed graphs: adds qualifying nodes to the vectors Extension and VSubgraph
//base case: VSubgraph length =size k
void extendSubgraphDirected(const PNGraph& Graph, 
                            TVec<TInt>& VSubgraph, 
                            TVec<TInt>& VExtension, 
                            //const TInt& graphSet, 
                            set* gv, 
                            const int G_M,
                            const int G_N,  
                            graph* nautyGraph, 
                            int numNodes,
                            int* lab, 
                            int* ptn, 
                            set* theSet, 
                            int* orbits, 
                            optionblk& options,
                            statsblk& stats, 
                            setword* workspace, 
                            int maxM, 
                            graph* canon, int targetNode)
{
    vector<int> id;
    //if a subgraph is found
    if(VSubgraph.Len() ==  G_N)
    {

        for(int i = 0; i < VSubgraph.Len(); i++)
        {
            id.push_back(int(VSubgraph[i]) );   
        }

        //printVector("FOUND==================: ", VSubgraph);


        //for each node in subgraph that corresponds to graphrow in nautyGraph call editNautyGraph
            for(int i = 0; i < G_N; i++)
            {
                editNautyGraphDirected(i, Graph, VSubgraph, G_M, gv, G_N, 
                                    nautyGraph, numNodes, 
                                    lab,  ptn, NILSET,  orbits, options,
                                    stats, workspace, 160 * MAXM,  canon);
            } 
        
        //NAUTY call for canonical labelling
        nauty(nautyGraph, lab, ptn, NILSET, orbits, &options, &stats, workspace, 160 * MAXM, G_M, G_N, canon);

        addToLabelCountHash(canon, gv, G_N, G_M, id);
        
        if(hashType == "subgraph" && !randGraph)
        { 
            //addToLabelNodeHash(canon, gv, G_N, G_M, id);
        }
 
        //after each subgraph clear the set data for each graphrow
        for (int i = 0; i != G_N; ++i) 
        {
            EMPTYSET(GRAPHROW(nautyGraph, i, G_M), G_M);
        }
        VSubgraph.DelLast();
        if(!randGraph)
        {
            subgraphCounter++;
        }
        else
        {
            randomCounter++;
        }
        return;
    }        

    while(VExtension.Len() != 0)
    {
        TVec<TInt> Extension; 

        int currNode = VExtension[0];
        VSubgraph.Add(currNode);
        VExtension.Del(0);

        GetAdjacenciesOfNodesInSubgraphDirected(Graph, VSubgraph, Extension, targetNode, currNode, VExtension);

        
        extendSubgraphDirected(Graph,
                                VSubgraph, 
                                Extension,                    
                                gv, 
                                G_M,
                                G_N, 
                                nautyGraph,                  
                                numNodes,
                                lab, 
                                ptn, 
                                gv, 
                                orbits, 
                                options, 
                                stats, 
                                workspace, 
                                maxM, 
                                canon, targetNode);
    }
    VSubgraph.DelLast();
}

void getEdge(int targetNode, TVec<TInt> VExtension, const PNGraph& Graph)
{
    int edge = 0;
    for (TNGraph::TNodeI NID = Graph->BegNI(); NID < Graph->EndNI(); NID++) 
    {
        edge = NID.GetId();

        if(edge > targetNode)
        {
            if(Graph->IsEdge(edge, targetNode) ||  Graph->IsEdge(targetNode, edge) )
            {
                VExtension.Add(edge);
                    
            }
        }
    }
}

//for DSirected graphs: adds qualifying nodes to the vectors Extension and VSubgraph
//base case: VSubgraph length =size k
void enumerateSubgraphsDirected(const PNGraph& Graph,
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{  

    TVec<TInt> VSubgraph;
    TVec<TInt> VExtension;

    int targetNode = 0;
    int isEdge = 0;

    for (TNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) 
    {
        callCount++;
            
        if((numNodes/10) == 0)
        {
            //cerr<< "Original Graph Total Progress: " << callCount << "/" << numNodes << endl;
        }

        VSubgraph.Clr(true, -1);
        VExtension.Clr(true, -1);
        targetNode = NI.GetId();
        VSubgraph.Add(targetNode);
        isEdge = 0;

        for (TNGraph::TNodeI NID = Graph->BegNI(); NID < Graph->EndNI(); NID++) 
        {
            isEdge = NID.GetId();

            if(isEdge > targetNode)
            {
                if(Graph->IsEdge(isEdge, targetNode) ||  Graph->IsEdge(targetNode, isEdge) )
                {
                    VExtension.Add(isEdge);
                        
                }
            }
        }

        extendSubgraphDirected(Graph, VSubgraph, VExtension,  

                                gv, 
                                G_M, 
                                G_N, 
                                nautyGraph, 
                                numNodes, 
                                lab, 
                                ptn, 
                                NILSET, 
                                orbits, 
                                options,
                                stats, 
                                workspace, 
                                160 * MAXM, canon, targetNode);  
    }
}


void randomDirected(const PNGraph& Graph, int numGraphs, 
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{
    for(int i = 0; i < numGraphs; i++)
    {
        TIntV resultDegree;
       
        //get orignal graph deg sequence vector
        TSnap::GetDegSeqV(Graph, resultDegree);
        TRnd rand = (Graph->GetNodes(), 0);
        PUNGraph graph = dirMethod(resultDegree, rand);
        PNGraph randGraph = TSnap::ConvertGraph<PNGraph, PUNGraph>( graph, false);

        enumerateSubgraphsDirected(randGraph, 
                                         gv, 
                                         G_M, 
                                         G_N, 
                                         nautyGraph, 
                                         numNodes, 
                                         lab, 
                                         ptn,
                                         NILSET, 
                                         orbits, 
                                         options,
                                         stats, 
                                         workspace, 
                                         160 * MAXM, 
                                         canon);
    }
}
//END DIRECTED FUNCTIONS///////////////////////////////////////////
/* 
void getAdjacencies(PUNGRAPH& Graph, TVec<TInt>& VSubgraph, TVec<TInt>& myExtension, int targetNode, const int  G_N, int currNode)
{

    const TUNGraph::TNodeI NI = graph->GetNI(currNode);
    int deg = NI.GetOutDeg();

    //iter over edges 
    for(int j = 0; j < deg; j++)
    {
        int outNodeId = NI.GetOutNId(j);//current edge 

        if(targetNode < outNodeId)
        {
            for(int k = 0; k < subgraph.Len(); k++)
            {   
                if(!graph->IsEdge(subgraph[k], outNodeId ))
                {
                    if( ! TVecContains(subgraph, outNodeId) &&
                        ! TVecContains(Extension, outNodeId) )
                    {
                        Extension.Add(outNodeId);  
                    }    
                }
            } 
        }
    }
}

void extend(const PUNGraph& Graph, 
                                TVec<TInt>& VSubgraph, 
                                TVec<TInt>& Extension, 
                                const int G_N, 
                                 int targetNode)
{

    TVec<TInt> myExtension;
    vector<int> id;

    //if a subgraph is found add to id vector for labelNodeIdMap
    if(VSubgraph.Len() ==  G_N)
    {
       for(int i = 0; i < VSubgraph.Len(); i++)
       
           id.push_back(int(VSubgraph[i]) );   
       }
        VSubgraph.DelLast();
    }         
    
        int currNode = 0;
        currNode = Extension[0];
        VSubgraph.Add(currNode);
        Extension.Del(0);
        
        for(int i = 0; i < Extension.Len(); i++)
        {
            myExtension.Add(Extension[i]);
        }

        getAdjacencies(Graph, VSubgraph, myExtension, targetNode, G_N, currNode);
        
    extendSubgraphUndirected(Graph,
                                VSubgraph, 
                                Extension, 
                                G_N, 
                                 targetNode);
    }
    VSubgraph.DelLast();
}



void enumerate(const PUNGraph& Graph,
                                    const int G_N) 
{  
    TVec<TInt> VSubgraph;
    TVec<TInt> VExtension;

    //if ORIGINAL graph or hashType = subgraphs
    if(!randGraph)
    {
        for (TUNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) 
        {
            VSubgraph.Clr(true, -1);
            VExtension.Clr(true, -1);
            TSnap::DelSelfEdges(Graph);
            int targetNode = NI.GetId();
            VSubgraph.Add(targetNode);

            //iterate thru the targetNode's edges
            for (int e = 0; e < NI.GetOutDeg(); e++)
            {
                if(NI.GetId() < NI.GetOutNId(e))
                {
                    VExtension.Add(NI.GetOutNId(e)); 
                }
            }
        
            extend(Graph, VSubgraph, VExtension,   
                                G_N, 
                                 targetNode);  
        }
    }   
}


//+++++++++++++++++++++++++++++++++

*/

//main
int main(int argc, char* argv[])
{
    Env = TEnv(argc, argv, TNotify::StdNotify);
    TExeTm ExeTm;
    Try
    for(int i = 0; i < 8; i++)
    {
        prob.Add(50);
    }
    const TStr InFNm = Env.GetIfArgPrefixStr("-i:", "snapTestData.txt", "Input graph");
    const TStr GraphType = Env.GetIfArgPrefixStr("-g:", "Graph Type: Undirected", "Graph type");
    OutFNm = Env.GetIfArgPrefixStr("-o:", "output.txt", "Output file name prefix");
    const TInt k = Env.GetIfArgPrefixInt("-k:", 3, "Subgraph size");   
    const TStr nemoCount = Env.GetIfArgPrefixStr("-c:", "nemoCount: n", "NemoCount");   
    const TStr HashType = Env.GetIfArgPrefixStr("-s:", "Hash Type: LabelCount", "Hash type");
    const TStr graphviz = Env.GetIfArgPrefixStr("-p:", "graphviz: n", "GraphViz png");
    const TInt Networks = Env.GetIfArgPrefixInt("-r:", 1000, "Number of random graphs to generate");  
    int numNodes = 0;
    int numEdges = 0;
    s_nemoCount = nemoCount;
    networks = Networks;
    inFile = InFNm;
    s_graphviz = graphviz;
    hashType = HashType;
	bool directed = true;
	short G_N = k;
    srand(time(NULL));
    totalGraphs = Networks +1;
    initCountSubg();  
    initConcentration();
   
    const int G_M = (G_N + WORDSIZE - 1) / WORDSIZE;
    graph canon[MAXN * MAXM];
    graph nautyGraph[MAXN * MAXM];
    for (int i = 0; i != G_N; ++i)
    {
		EMPTYSET(GRAPHROW(nautyGraph, i, G_M), G_M);
    }
	int lab[MAXN], ptn[MAXN], orbits[MAXN];
    static DEFAULTOPTIONS(options);
    statsblk(stats);
    setword workspace[160 * MAXM];
    set *gv = NULL;
    options.writeautoms = FALSE;
    options.getcanon = TRUE;
    if (directed)
    {
		options.digraph = TRUE;
		options.invarproc = adjacencies;
		options.mininvarlevel = 1;
		options.maxinvarlevel = 10;
    }
    nauty_check(WORDSIZE, G_M, G_N, NAUTYVERSIONID);
    cout << endl; 
    gettimeofday(&start, NULL);

    outfile = new std::ofstream(OutFNm.CStr());

    //
    if(GraphType == "directed")
    {
        cerr << "DIRECTED" << endl;
        PNGraph Graph = TNGraph::New();
        Graph = TSnap::LoadEdgeList<PNGraph>(InFNm); 
        TSnap::DelSelfEdges(Graph);
        numNodes = Graph->GetNodes();
        printf("nodes:%d  edges:%d\n", Graph->GetNodes(), Graph->GetEdges());
        cout << endl;

        enumerateSubgraphsDirected(Graph,                                   
                                    gv,  
                                    G_M, 
                                    G_N,   
                                    nautyGraph, 
                                    numNodes, 
                                    lab, 
                                    ptn, 
                                    NILSET, 
                                    orbits, 
                                    options,
                                    stats, 
                                    workspace, 
                                    160 * MAXM, 
                                    canon);

        if(hashType == "subgraph")
        {
            printLabelNodeHash();
        }

        if(Networks > 0)
        {
            randGraph = true;
            randomDirected(Graph, Networks,
                                         gv, 
                                         G_M, 
                                         G_N, 
                                         nautyGraph, 
                                         numNodes, 
                                         lab, 
                                         ptn,
                                         NILSET, 
                                         orbits, 
                                         options,
                                         stats, 
                                         workspace, 
                                         160 * MAXM, 
                                         canon);
        }
        printLabelCountHashDirected();
    }
    else
    {
        PUNGraph Graph = TUNGraph::New();
        Graph = TSnap::LoadEdgeList<PUNGraph>(InFNm);
        TSnap::DelSelfEdges(Graph);
        numNodes = Graph->GetNodes();
        numEdges = Graph->GetEdges();
        maxId = Graph->GetMxNId(); 
        printf("nodes:%d  edges:%d\n", numNodes, Graph->GetEdges());
        cout << endl;
        enumerateSubgraphsUndirected(Graph,
                                    gv,  
                                    G_M, 
                                    G_N,  
                                    nautyGraph, 
                                    numNodes, 
                                    lab, 
                                    ptn, 
                                    NILSET, 
                                    orbits, 
                                    options,
                                    stats, 
                                    workspace, 
                                    160 * MAXM, 
                                    canon);


        original = false;
        if(Networks > 0)
        {
            randGraph = true;
            randomUndirected(Graph, 
                                 gv, 
                                 G_M, 
                                 G_N, 
                                 nautyGraph, 
                                 numNodes, 
                                 lab, 
                                 ptn,
                                 NILSET, 
                                 orbits, 
                                 options,
                                 stats, 
                                 workspace, 
                                 160 * MAXM, 
                                 canon);
        }
        randGraph = false;
        printLabelCountHash(Graph, G_N);
//TODO


        gettimeofday(&end, NULL);
        long seconds  = end.tv_sec  - start.tv_sec;
        long useconds = end.tv_usec - start.tv_usec;
        long long mtime = ((seconds * 1000)  + useconds/1000.0) ;
        totalTime = double(mtime) /double(1000);

        TVec<TInt> edge;

        gettimeofday(&start3, NULL);

        if(hashType == "subgraph")
        {
            hashType = "none";
            generateLabelSubgraphs(Graph, 
                               gv, 
                               G_M, 
                               G_N, 
                               nautyGraph, 
                               numNodes, 
                               lab, 
                               ptn,
                               NILSET, 
                               orbits, 
                               options,
                               stats, 
                               workspace, 
                               160 * MAXM, 
                               canon);
        }
        gettimeofday(&end3, NULL);
        //time for subgraphCollection
        long seconds3  = end3.tv_sec  - start3.tv_sec;
        long useconds3 = end3.tv_usec - start3.tv_usec;
        long long mtime3 = ((seconds3 * 1000)  + useconds3/1000.0) ;
        nemoCountTime = double(mtime3) / double(1000); 

        //calc nemoProfile time
        gettimeofday(&start4, NULL);

        //print nemoProfile(labelNodeIdCount)
        
       cerr << "HIT" << endl; 
            printLabelIdHash();
        

        gettimeofday(&end4, NULL);
        long seconds4  = end4.tv_sec  - start4.tv_sec;
        long useconds4 = end4.tv_usec - start4.tv_usec;
        mtime4 = ((seconds4 * 1000)  + useconds4/1000.0) ;
    }
 
    double rand = double(randTime) /double(1000) ;//seconds
    double graphvizTime = double(mtimeGraphviz) / double(1000); 
    double nemoProfileTime = double(mtime4) / double(1000);//seconds 
    int totalSubgraphCount = randomCounter + subgraphCounter;

    *outfile<< "Nodes: " << numNodes << "  Edges: " << numEdges << endl;
    *outfile << " subgraph size: " << k << endl; 
    *outfile << endl << subgraphCounter << " subgraphs were enumerated in the original network. " << endl; 
    *outfile << randomCounter << " subgraphs were enumerated in the random networks. " << endl; 
    *outfile << totalSubgraphCount << " subgraphs were enumerated in all networks. " << endl << endl; 
    *outfile << "Total Time:  " << totalTime - graphvizTime << "  seconds." <<  endl;
    *outfile << "Randomization took " << randTime  << "  milliseconds." << rand << endl;    
    *outfile << "Enumeration took " << totalTime - rand  - graphvizTime << "  seconds." <<  endl;
    *outfile << "NeMoCounting time:  " << nemoCountTime  << "  seconds." <<  endl;
    *outfile << "NemoProfile Time: " <<  nemoProfileTime  << " seconds." <<  endl;
    *outfile << "Graphviz Time: " <<  graphvizTime  << " seconds." <<  endl;
    *outfile << "Total time with nemoProfile: " <<  totalTime + nemoProfileTime - graphvizTime << "  seconds." <<  endl;
    *outfile << "Total time with nemoProfile and nemoCounting: " <<  totalTime + nemoProfileTime - graphvizTime + nemoCountTime << "  seconds." <<  endl;
    *outfile << "Total Time with nemCounting, nemoProfile, and graphviz: " << totalTime + nemoCountTime + nemoProfileTime   << " milliseconds." <<  endl;
    outfile->close();
    delete outfile;
    Catch
    return 0;
}

