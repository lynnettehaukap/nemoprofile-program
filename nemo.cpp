//nemo.cpp
//Description: 
//Implementation of Fanmod program with additional nemoProfile, nemoCollect, nemoCount output 
//Known Bugs: 
//1) The nemoProfile and subgraphProfile need adapting for output to a file with a max of 
// five or so nauty labels per file in order to be legible with larger motif sizes
//2) The direct methods no longer work. The SNAP library does not support random graph generation with a preserved degree sequence for directed methods, so work on the directed methods was abandoned
#include <iostream>
#include <list>
#include <iomanip>
#include <vector>
#include <fstream>
#include "stdafx.h"
#include <map>
#include <assert.h>
#include <string.h>
#include "nautyinit.h"
#include <cstdlib>
#include <time.h>
#include <cstdlib>
#include <string>
#define DIRNAME "graphviz"
using namespace std;

static int size = 0;
static bool original = true;//original graph enumeration
typedef uint64 graph64;//Nauty label
static std::map< graph64, std::vector< std::vector< int >  > > subgraphIdMap;
static TStr s_nemoCount = NULL;
static TStr s_nemoProfile = NULL;
static TStr s_nemoCollection = NULL;
static TStr s_subgraphCount = NULL;
static TStr s_subgraphProfile = NULL;
static TStr s_nemoInputFile = NULL;
static TStr s_subgraphCollection = NULL;
static long long mtimeGraphviz = 0; //to calc graphviz time
//static double countingTime = 0;//for nemoCounting time calc
static long long mtime4 = 0;//for nemoProfile time calc
static int maxId = 0;//for printing width
static double totalTime = 0; //time elapsed with graphviz and withOUt nemocollection, nemoprofile, nemocounting
static int nemoCollectionTime = 0;
static int nemoCountTime = 0;//to calc nemoCountTime
static vector< int > labelNodeIdCountArr;//used to create nemoProfile
static vector< int > labelNodeIdCount2;//used to create subgraphProfile
static std::map<int, vector<int> > LabelNodeIdMap;//stores nauty label and count for each node id(used with neMoProfile)
//static std::map<int, vector<int> > LabelNodeIdMap2;//stores  label and count for each node id(used with subgraphProfile)
static int randTime = 0; //total time for random generation in seconds
static int randMsTotal = 0; //total time for random generation in ms
static int probIter = 0;//used to calculate random graph progress
static int labelIter = 0; //used to iter over labelId that contains sig motif labels
static int totalGraphs = 0;//total graphs for calculating output.txt data (freq, etc)
static std::vector<graph64> labelId; //the significant motifs
static uint64 subgraphCounter = 0;  //counts total num of subgraphs enumerated
static std::map<graph64, map<int, int> > nodeIdMap;// nauty Label, nodeid and count used for nemoProfile, nemCollection, 
typedef std::vector<uint64> ResultCount; //array of nauty label count for each graph
static int labelCountPtr = 0;//iterates over labelCountmap
typedef std::map< graph64, ResultCount > LabelCountMap; //nautyLabel, arr of label count for each graph; used for nemoCounting and freq calculations 
static LabelCountMap labelMap; //instance of Labelcountmap
//static float count= 0;
static int networks = 0;//num of rand networks
static int callCount = 0;//used to calc progress of orig graph
static int randCount = 0;//used to calc progress of rand graphs
static TStr inFile = NULL;// input file
static TStr s_graphviz = NULL;
static int randomCounter = 0;//used to iter over rand graphs
static int networkCount = 0;//used to calc progress of rand graph generation
static TStr hashType = NULL;//used when subgraph data will be collected using rand graphs
bool randGraph = false;//determines if collecting orig or rand graph data
TVec<TInt> prob;//used to calc randgraph progress
static TVec<TInt> countSubgr; //for each graph stores the total subg's found
static int countSubgrPtr = 0;//iter for countSubgr
//static int totalSubgr = 0;
static TVec<TFlt> concentration;//stores the frequencey for each graphs
static TVec<TInt> randomIds;//contains nodeids for random graph used for random generation
static vector< vector<int> > labelNodeIdVec;// used for graphviz images
string vectorOutput = "vectorOutput.txt";
static TStr nemoCounting = NULL;//used with optional output file
static TStr nemoCollect = NULL;//for use with output file param
static TStr nemoProfile2 = NULL;//use with output file param
static TStr subgraphCounting = NULL;//use with output file
static TStr subgraphCollect = NULL;//used with output file param
static TStr subgraphProfile2 = NULL;//used with output file param
static TStr graphViz2 = NULL;//used with output file parameter
std::ofstream *outfile = NULL;
std::ofstream vectorOutputFile(vectorOutput.c_str());//used with printVector 
std::ofstream *nemoCountingFile = NULL; 
std::ofstream *nemoCollectionFile = NULL; 
std::ofstream *nemoProfileFile = NULL; 
std::ofstream *subgraphCountFile = NULL; 
std::ofstream *subgraphCollectionFile = NULL; 
std::ofstream *subgraphProfileFile = NULL; 
static TStr OutFNm =NULL;
struct timeval start, start1, start2, start3, start4, start5, end, end1, end2, end3, end4, end5;
typedef std::map< graph64, std::vector< std::vector<int> > > LabelNodeMap; //nauty label and subgraph ids
static LabelNodeMap subgraphIds;// used for nemoCollection to print subgraphs

//contains output.txt calculation data
struct subgr_res
{
    graph64 id;
    double freq;
    double randMean;
    double randSd;
    double pValue;
    double zScore;
};


//BEGIN HELPER FUNCTIONS////////////////////
//prints vector to stdout
void printVector( const TVec<TInt>& vector)
{
    for(int i = 0; i < vector.Len(); i++)
    {
       cerr <<  vector[i] << " ";    
       vectorOutputFile <<  vector[i] << " ";    
    }
    cerr <<  endl;
   // randOutfile <<  endl;
}


//checks vector for node id: returns true if found
bool TVecContains( const TVec<TInt>& vec, const int id)
{
    for(int j = 0; j <  vec.Len(); j++)
    {
        if(id == vec[j])
            return true;            
    }   

    return false;
}

//intializes 2d array to null
void init2dArr(int** arr, const int size)
{

    for( int i=0; i<size; ++i)
    {
        arr[i] = new int[size];
    } 
    
    //init neighborArr to zero
    for(int i = 0; i < size; i++)
    {
        for(int j = 0; j < size; j++)
        {
            arr[i][j] = 0;
        } 
    }
}

//initialize nodeId arr with null
void initNodeIds(vector<int>& nodeIds, int numNodes)
{
    for(int i = 0; i <= numNodes; i++)
    {
        nodeIds.push_back(0);
    }
}

TVec<TInt> convertIntVec(vector<int> vec)
{
    TVec<TInt> subgraphs;
    for(unsigned int i = 0; i < vec.size(); i++)
    {
        subgraphs.Add(vec[i]);
    }
    return subgraphs;
}

//END HELPER FUNCTIONS////////////////////

//SNAP function without printf statements
PUNGraph dirMethod(const TIntV& DegSeqV, TRnd& Rnd)
{

    const int Nodes = DegSeqV.Len();
     PUNGraph GraphPt = TUNGraph::New();
     TUNGraph& Graph = *GraphPt;
    Graph.Reserve(Nodes, -1);
    TIntV NIdDegV(DegSeqV.Len(), 0);
   int DegSum=0, edges=0;
   for (int node = 0; node < Nodes; node++) {
        Graph.AddNode(node);
    for (int d = 0; d < DegSeqV[node]; d++) { NIdDegV.Add(node); }
   DegSum += DegSeqV[node];
   }
   NIdDegV.Shuffle(Rnd);
  TIntPrSet EdgeH(DegSum/2); // set of all edges, is faster than graph edge lookup
     int u=0, v=0;
     for (int c = 0; NIdDegV.Len() > 1; c++) {
       u = Rnd.GetUniDevInt(NIdDegV.Len());
       while ((v = Rnd.GetUniDevInt(NIdDegV.Len())) == u) { }
       if (u > v) { Swap(u, v); }
       const int E1 = NIdDegV[u];
       const int E2 = NIdDegV[v];
       if (v == NIdDegV.Len()-1) { NIdDegV.DelLast(); } 
       else { NIdDegV[v] = NIdDegV.Last();  NIdDegV.DelLast(); }
       if (u == NIdDegV.Len()-1) { NIdDegV.DelLast(); } 
       else { NIdDegV[u] = NIdDegV.Last();  NIdDegV.DelLast(); }
       if (E1 == E2 || EdgeH.IsKey(TIntPr(E1, E2))) { continue; }
       EdgeH.AddKey(TIntPr(E1, E2));
       Graph.AddEdge(E1, E2);
       edges++;
     }
     return GraphPt;
}

graph64 getLabel(graph* canon, set* gv, const int G_N, const int G_M)
{

    //Fanmod code to retrieve canon data
    graph64 res_gr = 0ULL;
    for (int a = 0; a < G_N; ++a) 
    {
        gv = GRAPHROW(canon, a, G_M);
	    for (int b = 0; b < G_N; ++b) 
        {
		    res_gr <<= 1;
		    if (ISELEMENT(gv, b))
            {
		        res_gr |= 1;
		    }
	    }
    }
    return res_gr;
}

//initialize resultVec to null
void initResultVec(ResultCount& vec)
{
    for(int i = 0; i < totalGraphs; i++)
    {
        vec.push_back(0);
    }
}

//fanmod code to determine id printing width
//get number of digits of a unint64
int numDigits(uint64 i)
{
    int ret = 1;
    while(i > 9)
    {
        i /= 10;
        ++ret;
    }
    return ret;
}

//print nemoCounting: prints total number of instances found for each significant nauty motif
//print subgraphCount also
void printNemoCounting()
{
    if(s_subgraphCount == "y")
    {
        subgraphCountFile = new std::ofstream(subgraphCounting.CStr());
        int maxId = 10;

         LabelCountMap::iterator iter = labelMap.begin();

        //print all motif labels  and their subgraph counts
        for( ; iter!= labelMap.end(); iter++)
        {
            graph64 id = iter->first;
            vector< uint64 > count = iter->second;
            *subgraphCountFile << setw(maxId) << "id: " << setw(maxId) << id <<  endl;
            *subgraphCountFile <<  setw(maxId) << "count: " << setw(maxId) << count[0] << endl <<  endl;
        }
        subgraphCountFile->close();
        delete subgraphCountFile;
    }

    //print all significant motif label subgraph counts
    if(s_nemoCount == "y")
    {

        nemoCountingFile = new std::ofstream(nemoCounting.CStr());
        *nemoCountingFile << s_nemoInputFile.CStr() << " Size:" << size << " Random Graphs:" << networks << endl <<endl;
        int maxId = 10;

        //print each  motif label from list
        for(unsigned int i = 0; i < labelId.size(); i++)
        {
            LabelCountMap::iterator iter = labelMap.find(labelId[i]);
            graph64 id = iter->first;
            vector< uint64 > count = iter->second;
            *nemoCountingFile << setw(maxId) << "id: " << setw(maxId) << id <<  endl;
            *nemoCountingFile <<  setw(maxId) << "count: " << setw(maxId) << count[0] << endl <<  endl;
        }
        nemoCountingFile->close();
        delete nemoCountingFile;
    }
}

//helper for printLabelIdHash-nemoProfile
//writes to file 1 page of label-counts
unsigned int nemoProfileHelper(unsigned int start, unsigned int end )
{
    std::map<int, vector<int> >::iterator it2 = LabelNodeIdMap.begin();    
    const int nodeIdWidth = 10;

    //write to file the labels
    for(unsigned int i = start; i < end; i++)
    {   
        *nemoProfileFile << labelNodeIdCountArr[i] << setw(nodeIdWidth) << "  " ;
    }
    *nemoProfileFile << endl;

    //write the count values to file
    for( ; it2 != LabelNodeIdMap.end(); it2++)
    {
        vector< int >  myVec = it2->second;
        *nemoProfileFile << setw(nodeIdWidth) << it2->first << "  ";
                
        //TODO
        for(unsigned int j = start; j <= end; j++)
        {
            *nemoProfileFile <<  setw(nodeIdWidth ) <<  myVec[j] << "     ";
        }       
        *nemoProfileFile << endl;
    }
    return end;
}

//add to node ids to subgraphIdMap
//print nemoProfile
void printLabelIdHash()
{
    if(s_subgraphProfile == "y")
    {

        subgraphProfileFile = new std::ofstream(subgraphProfile2.CStr());
        std::map<graph64, map<int, int> >::iterator iter = nodeIdMap.begin();
        uint64 maxId = 0;    
        const int nodeIdWidth = 10;
        subgraphProfileFile->precision(5);
        subgraphProfileFile->flags( std::ofstream::right);

        //get the max nauty id width for printing
        for( ; iter != nodeIdMap.end(); iter++)
        {    
            graph64 id = iter->first;
        
            if(id > maxId)
            {   
                maxId = id; 
            }
        }
        *subgraphProfileFile << "_____________________________________________________________________________________________________________" << endl;

        int size = nodeIdMap.size();


        std::map<int, vector<int> >::iterator it = LabelNodeIdMap.begin();
        //add each nauty id to  LabelNodeIdMap and initialize vector to null values
        for(it = LabelNodeIdMap.begin() ; it != LabelNodeIdMap.end(); it++)
        {    
            int idIter = it->first;
            vector<int> myVec = it->second;
    
            //initialize myVec to 0
            for(int i = 0; i < size; ++i)
            {   
                myVec.push_back(0);
            }
            LabelNodeIdMap[idIter]= myVec;
        }
        int vecIndex = 0;
    
        //get data from nodeIdMap and populate LabelNodeIdMap    
        for(iter = nodeIdMap.begin() ; iter != nodeIdMap.end(); iter++)
        {   
            graph64 idIter = iter->first;
            labelNodeIdCountArr.push_back(idIter);
            map<int, int> countMap = iter->second;
            std::map<int, int>::iterator innerIter = countMap.begin();    
            //iter over nodeidmap innermap and populate labelNodeidmap with the label count for each node id
            for( ; innerIter != countMap.end(); innerIter++)
            {   
                std::map< int, vector<int> >::iterator iter2 = LabelNodeIdMap.find(innerIter->first);
                std::vector< int >  myVec = iter2->second;
                myVec[vecIndex] = innerIter->second;
                iter2->second = myVec;
            }
            ++vecIndex;
        }
        std::map<int, vector<int> >::iterator it2 = LabelNodeIdMap.begin();
        *subgraphProfileFile << setw(nodeIdWidth) << "Node Id:" << "  "; 
        *subgraphProfileFile << setw(nodeIdWidth) << "ID:" << "  ";
        
        //print to file the labelNodeIdCountArr contents
        for(unsigned int i = 0; i < labelNodeIdCountArr.size(); i++)
        {
            *subgraphProfileFile << labelNodeIdCountArr[i] << setw(nodeIdWidth) << "  " ;
        }
        *subgraphProfileFile << endl;

        //print the contents of LabelNodeIdMap's values to file
        for( ; it2 != LabelNodeIdMap.end(); it2++)
        {
            vector< int >  myVec = it2->second;
            *subgraphProfileFile << setw(nodeIdWidth) << it2->first << "  ";
            //
            for(unsigned int j = 0; j < myVec.size(); j++)
            {
                *subgraphProfileFile <<  setw(nodeIdWidth ) <<  myVec[j] << "     ";
            } 
            *subgraphProfileFile << endl;
        }
        *subgraphProfileFile << "_____________________________________________________________________________________________________________" << endl;
        subgraphProfileFile->close();
        delete subgraphProfileFile;
    }

    //if random graphs calculated significant motifs iter over motifs and print
    if(s_nemoProfile == "y")
    {
        std::map<graph64, map<int, int> >::iterator iter = nodeIdMap.begin();
        uint64 maxId = 0;    
        const int nodeIdWidth = 10;

        //get the max nauty id width for printing
        for( ; iter != nodeIdMap.end(); iter++)
        {    
            graph64 id = iter->first;
        
            if(id > maxId)
            {   
                maxId = id; 
            }
        }

        int size = nodeIdMap.size();
        std::map<int, vector<int> >::iterator it = LabelNodeIdMap.begin();

        //add each nauty id to  LabelNodeIdMap and initialize vector to null values
        for(it = LabelNodeIdMap.begin() ; it != LabelNodeIdMap.end(); it++)
        {    
            int idIter = it->first;
            vector<int> myVec = it->second;
    
            //initialize myVec to 0
            for(int i = 0; i < size; ++i)
            {   
                myVec.push_back(0);
            }
            LabelNodeIdMap[idIter]= myVec;
        }
        int vecIndex = 0;
        //get data from nodeIdMap and populate LabelNodeIdMap    
        for(unsigned int i = 0; i < labelId.size(); i++)
        {
            std::map<graph64, map<int, int> >::iterator iter = nodeIdMap.find(labelId[i]);
            graph64 idIter = iter->first;
            labelNodeIdCountArr.push_back(idIter);
            map<int, int> countMap = iter->second;

            std::map<int, int>::iterator innerIter = countMap.begin();    
            //iter over nodeidmap innermap and populate labelNodeidmap with the label count for each node id
            for( ; innerIter != countMap.end(); innerIter++)
            {   
                std::map< int, vector<int> >::iterator iter2 = LabelNodeIdMap.find(innerIter->first);
                std::vector< int >  myVec = iter2->second;
                myVec[vecIndex] = innerIter->second;
                iter2->second = myVec;
            }
            ++vecIndex;
        }
        int counter = 1;//to iter over each set of 8 labels for one text file
        int fileNum = 1;
        unsigned int curr = 0;
        char* extension = ".txt";
        int len = nemoProfile2.Len() + 5;
        int numLabel = labelNodeIdCountArr.size();
        char str[len];
        //TStr index = NULL;

        while(curr < labelNodeIdCountArr.size() )
        {
            if(curr > 7)
            {//reset count and inc filename
                int base = sprintf(str, "%d", fileNum);
                strcat(str, nemoProfile2.CStr() );
       //         strcat(str, extension);
                nemoProfile2 = str;
            }
            nemoProfileFile = new std::ofstream(nemoProfile2.CStr());
            *nemoProfileFile << s_nemoInputFile.CStr() << " Size:" << size << " Random Graphs:" << networks << endl <<endl;
            nemoProfileFile->precision(5);
            nemoProfileFile->flags( std::ofstream::right);
            *nemoProfileFile << "_____________________________________________________________________________________________________________" << endl;
            //std::map<int, vector<int> >::iterator it2 = LabelNodeIdMap.begin();
            *nemoProfileFile << setw(nodeIdWidth) << "Node Id:" << "  "; 
            *nemoProfileFile << setw(nodeIdWidth) << "ID:" << "  ";
       
            if(numLabel <= 8)
            {
              curr = nemoProfileHelper(curr, numLabel);
            }
            if(numLabel - (curr+ 1) <= 8)
            {
                //numLabel - curr >= 8
                curr = nemoProfileHelper(curr, numLabel);   
            }       
            if(numLabel - (curr +1) > 8)
            {
                curr = nemoProfileHelper(curr, curr+8);
            }
/*             
            //write to file the labels
            for(unsigned int i = 0; i < labelNodeIdCountArr.size(); i++)
            {   
                *nemoProfileFile << labelNodeIdCountArr[i] << setw(nodeIdWidth) << "  " ;
            }
            *nemoProfileFile << endl;

            //write the count values to file
            for( ; it2 != LabelNodeIdMap.end(); it2++)
            {
                vector< int >  myVec = it2->second;
                *nemoProfileFile << setw(nodeIdWidth) << it2->first << "  ";
                //TODO
                for(unsigned int j = 0; j < labelId.size(); j++)
                {
                    *nemoProfileFile <<  setw(nodeIdWidth ) <<  myVec[j] << "     ";
                }       
                *nemoProfileFile << endl;
            }
*/
            *nemoProfileFile << "_____________________________________________________________________________________________________________" << endl;
            nemoProfileFile->close();
            delete nemoProfileFile;
            //curr++;
            fileNum++;
        }
    }
}

//add to node ids to subgraphIdMap
void addToSubgraphIdMap(graph* canon, set* gv, const int G_N, const int G_M, vector<int> id )
{
    graph64 res_gr = getLabel(canon, gv, G_N, G_M);
//cout<< "resgraph" << res_gr <<endl;
    std::map< graph64, std::vector< std::vector< int >  > >::iterator iter = subgraphIdMap.find( res_gr);
    //if key not found create vector and add it to map
    if(iter == subgraphIdMap.end())
    {
        std::vector< vector <int> > myMap;
        
       // for(unsigned int i = 0; i < id.size(); i++)
        {
            myMap.push_back(id);
        }
        subgraphIdMap[res_gr] = myMap;  
    }
    else
    {
        std::vector< vector <int> >  myMap = iter->second;
       // for(unsigned int i = 0; i < id.size(); i++)
        {
            myMap.push_back(id);
        }
        subgraphIdMap[res_gr] = myMap;  
    }
}

//print subgraphIdMap
void printSubgraphIds()
{

    subgraphCollectionFile = new std::ofstream(subgraphCollect.CStr());
    map< graph64, vector< vector<int> > >::iterator it;
       
    //iter through subgraphIds map
    for(it = subgraphIdMap.begin(); it != subgraphIdMap.end(); ++it)
    { 
        std::vector< std::vector<int> > outerVector = it->second;
        int outerSize = outerVector.size();
        *subgraphCollectionFile<< "Label: " << it->first << endl;

        //iter through outer vector
        for( int i=0; i<outerSize; i++)
        {
            std::vector<int> innerVector = outerVector[i];
            *subgraphCollectionFile << "{";
            int innerSize = innerVector.size();

            //iter through inner vector and print subgraph node ids
            for( int j = 0; j<innerSize; j++)
            {
                *subgraphCollectionFile << innerVector[j];
                if(j != (innerSize -1) )
                {
                    *subgraphCollectionFile << ",";
                }
            }
            *subgraphCollectionFile << "}" << endl;
        }
        *subgraphCollectionFile <<  endl;                   
    }
}
//add/increment each id count for each nauty label to nodeIdMap
void addToLabelIdHash2(graph* canon, set* gv, const int G_N, const int G_M, vector<int> id )
{
    graph64 res_gr = getLabel(canon, gv, G_N, G_M);
//cout<< "resgraph" << res_gr <<endl;
    std::map<graph64, map<int, int> >::iterator iter = nodeIdMap.find( res_gr);
    //if key not found create vector and add it to map
    if(iter == nodeIdMap.end())
    {
        std::map<int, int> myMap;
        
        for(unsigned int i = 0; i < id.size(); i++)
        {
            myMap[id[i]]++;
        }
        nodeIdMap[res_gr] = myMap;  
    }
    else
    {
        std::map< int, int >  myMap = iter->second;
        for(unsigned int i = 0; i < id.size(); i++)
        {
            myMap[id[i]]++;
        }
        nodeIdMap[res_gr] = myMap;  
    }
}

//prints nemoCollection data
void printLabelSubgraphs(PUNGraph Graph, int labelIter, vector<int> id, graph* canon, set* gv, const int G_N, const int G_M)
{
    graph64 res_gr = getLabel(canon, gv, G_N, G_M);
    
    if(id.size() > 0)
    {
        //
        if(res_gr == labelId[labelIter])
        {
            *nemoCollectionFile << "ID: " << labelId[labelIter] << endl;
            for(unsigned int i = 0; i < id.size(); i++)
            {
                *nemoCollectionFile << id[i] << ", ";
            }
            *nemoCollectionFile << endl;
            *nemoCollectionFile << endl;
        }
    }
}

//increment nauty label count for each graph
void addToLabelCountHash(graph* canon, set* gv, const int G_N, const int G_M, vector<int> id)
{

    graph64 result  = getLabel(canon, gv, G_N, G_M);
    LabelCountMap::iterator iter = labelMap.find( result);
    
    //if key not found create vector and add it to map
    if(iter == labelMap.end())
    {
        ResultCount vec; 
        initResultVec(vec);       
        vec[labelCountPtr]++;
        labelMap[result] = vec; 
        labelNodeIdVec.push_back(id);
 
    }
    else
    {
        ResultCount vec = iter->second;
        vec[labelCountPtr]++;
        labelMap[result] = vec;  
    }
}

//from fanmod function
double calcMeanFreq(TVec<TFlt>& vec, int networks)
{
    if(networks == 0)
        return sqrt(-1.0);

    double mean = 0;
    for(int i = 1; i <= networks; ++i)
    {
        mean += vec[i];
    }
    return mean/networks;
}

//from fanmod function
double calcStdDeviation(TVec<TFlt>& vec, int networks, double randMean)
{
    if(networks == 0)
        return sqrt(-1.0);
    double deviation = 0;
    for(int i = 1; i <= networks; i++)
    {
        deviation += powf((vec[i] - randMean), 2);
    }
    deviation /= (networks -1);
    return sqrt(deviation);
}

//Nemo Collection: prints significant subgraphs
void printLabelNodeHash()
{
    LabelNodeMap::iterator it;
       
    //iter through subgraphIds map
    for(it = subgraphIds.begin(); it != subgraphIds.end(); ++it)
    { 
        std::vector< std::vector<int> > outerVector = it->second;
        int outerSize = outerVector.size();
        *outfile << "Label: " << it->first << endl;
        //iter through outer vector
        for( int i=0; i<outerSize; ++i)
        {
            std::vector<int> innerVector = outerVector[i];
            *outfile << "{";
            int innerSize = innerVector.size();

            //iter through inner vector and print subgraph node ids
            for( int j = 0; j<innerSize; ++j)
            {
                *outfile << innerVector[j];
                if(j != (innerSize -1) )
                {
                    *outfile << ",";
                }
            }
            *outfile << "}" << endl;
        }
        *outfile <<  endl;                   
    }
    map < graph64, uint64 >::iterator iter;
}

void printLabelCountHashDirected()
{
}

//undirected graphs only
void printLabelCountHash(const PUNGraph& Graph, const int G_N)
{
    int idx = 0;
    subgr_res gr;
    uint64 subgrCount = 0;//count for each label
    uint64 totalSubgrCount = 0;//total subgraph count for all labels in graph
    double subgraphs = 0.0;
    double totalSubgraphs = 0.0;
    LabelCountMap::iterator it;
    vector<subgr_res> resultVec(labelMap.size());
    int freqW = 10;
    int mFreqW = 13;
    int sdWidth = 14;
    int zWidth = 11;
    int pWidth = 9;
    uint64 maxId = 0;
    outfile->precision(5);
    outfile->flags( std::ofstream::right);
    //iter over labelMap 
    for(it = labelMap.begin(); it != labelMap.end(); ++it)
    { 
        ResultCount outerVector = it->second;
        gr.id = it->first;
        if(gr.id > maxId)
        {
            maxId = gr.id; 
        }
        subgrCount = outerVector[0];
    // cerr << "subgrCount " << subgrCount << endl;
        totalSubgrCount = countSubgr[0];
//cerr<< "totalSubgr " << totalSubgrCount << endl;
        subgraphs = (double) subgrCount;
//cerr << "subgrcount dbl " << subgraphs << endl;
        totalSubgraphs = (double) totalSubgrCount;
//cerr << "totalSubgr dbl " << totalSubgraphs << endl;
        gr.freq = (subgraphs / totalSubgraphs) ;
        concentration[0] = gr.freq;
//cerr << "________________" << endl << endl;
 
        if(networks > 0)
        {
            gr.pValue = 0;
            gr.randMean = 0;
            gr.randSd = 0;

            for( int i=1; i< totalGraphs; i++)
            {
                subgrCount = outerVector[i];
                totalSubgrCount = countSubgr[i];
                subgraphs = (double) subgrCount;
                totalSubgraphs = (double) totalSubgrCount;

//cerr << "con: " << concentration[i] << endl;
                if(subgraphs == 0.0 && totalSubgraphs == 0.0)
                {
                    concentration[i] = 0; 
                }
                else
                {
                    concentration[i] = (subgraphs / totalSubgraphs);
                }
 
                if(concentration[i] >= gr.freq)
                {
                    ++gr.pValue;
                }
            }
            gr.pValue /= double(networks);
            gr.randMean = calcMeanFreq(concentration, networks);                
            gr.randSd = calcStdDeviation(concentration, networks, gr.randMean);
//cerr << "mean: " << gr.randMean << endl;
//cerr << "sd: " << gr.randSd << endl;
            if(gr.randSd == 0)
            {
                gr.zScore = sqrt(-1);
            }
            else
            {
                gr.zScore = (gr.freq - gr.randMean) / gr.randSd;
            }
        }
        resultVec[idx++] = gr;
    }
    const int idWidth = numDigits(maxId);
    //Print results to file
    //labelNodeIdVec
    *outfile << "__________________________________________________________________" << endl;
    *outfile << setw(idWidth ) << "ID" << ' ' 
             << setw(freqW + 2) << "Frequency"; 

    if(networks > 0)
    {
        *outfile << setw(mFreqW + 1) <<  "Mean-Freq" << setw(sdWidth) << "Standard-Dev"
                 << setw(zWidth) << "Z-Score" << setw(pWidth) << "p-Value";
    }
        
    *outfile << endl;
    *outfile << setw(idWidth) << ' '  << setw(mFreqW ) << " [Original]";

    if(networks > 0)
    {
        *outfile << setw(mFreqW + 1) << "[Random]" << setw(sdWidth) << "[Random]";
    }
    *outfile << endl << endl;
            
    for(unsigned int i = 0; i < resultVec.size(); i++)
    {
        //datalines of the result table
        *outfile << setw(idWidth) << resultVec[i].id << ' ';
        *outfile << setw(freqW) << resultVec[i].freq * 100 << '%';
        
        if(networks > 0)
        {
            *outfile << setw(mFreqW) << resultVec[i].randMean * 100 << '%'
                     << setw(sdWidth) << resultVec[i].randSd
                     << setw(zWidth) << resultVec[i].zScore 
                     << setw(pWidth) << resultVec[i].pValue;
        }
        
        *outfile <<  endl << endl;
       
        //collect sig motif labels for nemoCollection 
        if(hashType == "subgraph")
        {
            if(resultVec[i].pValue < 0.01 && networks >= 1000 )
            {
                labelId.push_back(resultVec[i].id);
            }
            if(resultVec[i].zScore > 2.0 && networks < 1000)
            {
                labelId.push_back(resultVec[i].id);
            }
        }
        gettimeofday(&start2,NULL);  

        //print graphviz images for sig motifs 
        if(s_graphviz == "y")
        {
            TStr myString;
            //print label using graphviz
            if(labelId.size() != 0)
            {
                for(unsigned int i = 0; i < labelId.size(); i++)
                {
                    int id = int(labelId[i]);
                    // char* fileExt = ".png";
                    char strs[256] ;
                    int base = sprintf(strs, "%d", id );
                    char* extension = ".png";
                    myString = TStr(strs);
                    strcat(strs, extension);
                    TVec<TInt> vec = convertIntVec(labelNodeIdVec[i]);
                    PUNGraph subgraph = TSnap::GetSubGraph(Graph, vec);
                    TGVizLayout layout = (TGVizLayout)0;
                    bool label = false;
                    TStr fileName = TStr(strs);
                    //TStr fileName = "graphImages.png";
                    TIntStrH nidColor;
        
                    const TStr color = "white";
                    for(int i = 0; i < vec.Len(); i++)
                    {
                        nidColor.AddKey(i);
                        nidColor[i] = color;
                    }
                    TSnap::DrawGViz( subgraph, layout, fileName, myString, label, nidColor);
                }   
            }
        }
        gettimeofday(&end2,NULL);   
        long seconds2  = end2.tv_sec  - start2.tv_sec;
        long useconds2 = end2.tv_usec - start2.tv_usec;
        mtimeGraphviz = ((seconds2 * 1000)  + useconds2/1000.0) ;
    }
        *outfile << "__________________________________________________________________" << endl;
}

//initialize countSubgr to null
void initCountSubg()
{
    for(int i = 0; i < totalGraphs; i++)
    {
        countSubgr.Add(0);
    }
}

//init concentration arr to null
void initConcentration()
{
    for(int i = 0; i < totalGraphs; i++)
    {
        concentration.Add(0);
    }
}

//determine if node is an edge to nodes already in subgraph(to avoid getting repeat subgraphs)
bool isEdgeToNodes( const PUNGraph& graph, const TVec<TInt>& subgraph, int node)
{
  for( int i=0; i< subgraph.Len() ; ++i)
  {
    if(graph->IsEdge(subgraph[i], node))
      return true;
  }
  return false;
}
/*
//////BEGIN UNDIRECTED FUNCTIONS///////////////////////////////////////////
//finds the target node's edges in Undirected graph
void getAdjacenciesOfNodesInSubgraphUndirected(const PUNGraph& graph, 
                                     const TVec<TInt>& subgraph, 
                                     const TVec<TInt>& Extension,
                                     TVec<TInt>& currNodeAdj,
                                     int targetNode, int G_N,  int currNode)
{
//cerr << "SUBGRAPH: " ;
//printVector(subgraph);
//cerr << endl;
    const TUNGraph::TNodeI NI = graph->GetNI(currNode);
    int deg = NI.GetOutDeg();

    //determine if edge is valid for fanmod alg and add to valid adjacencies
    for(int j = 0; j < deg; j++)
    {
        int outNodeId = NI.GetOutNId(j);//current edge 

        //if edge node qualifies add to adj arr
        if(targetNode < outNodeId
            //not an edge to subgraph 
           //&& ! isEdgeToNodes( graph, subgraph, currNode )
           
           && ! TVecContains(currNodeAdj, outNodeId)
           && ! TVecContains(Extension, outNodeId)
           && ! TVecContains(subgraph, outNodeId) )
        {
            currNodeAdj.Add(outNodeId);
        }
    }

    //for rand graphs determine if edge is valid adn add to adj list if valid
    if(randGraph)
    {
        TVec<TInt> combined = Extension;
        for(int i = 0; i < currNodeAdj.Len(); ++i)
        {
          combined.Add(currNodeAdj[i]);
        }

        double randNum =  prob[probIter];
        double percent = randNum/100; 
        int randSubgraphEnum2 =  percent * combined.Len() + 0.5;
        while(combined.Len() > randSubgraphEnum2)
        {
            for(int i = 0; i < combined.Len(); i++)
            { 
                if(combined.Len() > randSubgraphEnum2) 
                {

                    int digit = rand() % 2;
                    if(digit)
                    {
                        combined.Del(i);
                    }
                } 
            }
        }
    }
}
*/

//make nauty call to set nauty graph before getting label
//find nodes call addelement on for each graphrow call
void editNautyGraphUndirected(int graphSet, const PUNGraph& Graph,
                    TVec<TInt> idSubg, 
                    const int G_M, 
                    set* gv, 
                    const int G_N,
                    graph* nautyGraph, 
                    int numNodes, 
                    int* lab, 
                    int* ptn, 
                    set* theSet, 
                    int* orbits, 
                    optionblk& options,
	                statsblk& stats, 
                    setword* workspace, 
                    int maxM, 
                    graph* canon)
{
//printVector(idSubg);
    PUNGraph subgraph = TSnap::GetSubGraph(Graph, idSubg);

     //call ADDELEMENT for each edge 
    for(int i = 0; i < G_N; i++)
    {
        for(int j = 0; j < G_N; j++)
        {
            if(subgraph->IsEdge(idSubg[i], idSubg[j]))
            {
                //NAUTY calls
                DELELEMENT(GRAPHROW(nautyGraph, i, G_M), j);
                gv = GRAPHROW(nautyGraph, i, G_M); 
                ADDELEMENT(gv, j);

            }
        }
    }
}

//set data needed to get Nauty labels and get the nauty label
void setNautyGraph(vector<int> id, TVec<TInt> idSubg, const PUNGraph& Graph, 
                                set* gv, 
                                const int G_M,
                                const int G_N, 
                                graph* nautyGraph, 
                                int numNodes,
                                int* lab, 
                                int* ptn, 
                                set* theSet, 
                                int* orbits, 
                                optionblk& options,
                                statsblk& stats, 
                                setword* workspace, 
                                int maxM, 
                                graph* canon ) 
{
    //edits subg data for nauty graph before calling nauty() and stores in nautyGraph
    for(int i = 0; i < G_N; i++)
    {
        editNautyGraphUndirected(i, Graph, idSubg, G_M, gv, G_N, 
                                    nautyGraph, numNodes, 
                                    lab,  ptn, NILSET,  orbits, options,
                                    stats, workspace, 160 * MAXM,  canon); 
   }

    //printVector(idSubg);

    //gets Nauty label id for the graph in nautygraph
    nauty(nautyGraph, lab, ptn, NILSET, orbits, &options, &stats, workspace, 160 * MAXM, G_M, G_N, canon); 
    
    countSubgr[countSubgrPtr]++;
    
    addToLabelCountHash(canon, gv, G_N, G_M, id);
/*
    if(hashType == "subgraph" && !randGraph && s_subgraphCollection == "y" )
    {       
        addToSubgraphIdMap(canon, gv, G_N, G_M, id);
    }
*/
    //add node id's and the label count to nodeIdMap by calling addToLabel...
    if(hashType == "subgraph" && !randGraph)
    {       
        addToLabelIdHash2(canon, gv, G_N, G_M, id);
    }
    if(hashType == "none")
    {
        printLabelSubgraphs(Graph, labelIter,  id, canon, gv, G_N, G_M);
    }

    //after each subgraph clear the set data for each graphrow
    for (int i = 0; i != G_N; ++i) 
    {
        EMPTYSET(GRAPHROW(nautyGraph, i, G_M), G_M);
    }
}

//sets nauty graph data 
void printEnumeratedSubgraphs( TVec<TInt>& subg, TVec<TInt>& ext, const PUNGraph& Graph, 
                                set* gv, 
                                const int G_M,
                                const int G_N, 
                                graph* nautyGraph, 
                                int numNodes,
                                int* lab, 
                                int* ptn, 
                                set* theSet, 
                                int* orbits, 
                                optionblk& options,
                                statsblk& stats, 
                                setword* workspace, 
                                int maxM, 
                                graph* canon) 
{
    int numExtensions = ext.Len();
    TVec<TInt> idSubg;

    //setNauty graph data
    for (int i=0; i<numExtensions; ++i)
    {
        vector<int> id;
        subg.Add(ext[i]);

        for(int i = 0; i < subg.Len(); i++)
        {
            id.push_back(subg[i] );  
            idSubg.Add(subg[i]);
        }
        setNautyGraph(id, idSubg, Graph, gv, G_M, G_N, 
                                    nautyGraph, numNodes, 
                                    lab,  ptn, NILSET,  orbits, options,
                                    stats, workspace, 160 * MAXM,  canon);

        //add subgraph ids to subgraph id map for subgCollect,subgCount, subgProfile
        if(hashType == "subgraph" && !randGraph && s_subgraphCollection == "y" )
        {       
            addToSubgraphIdMap(canon, gv, G_N, G_M, id);
        }
        
        idSubg.Clr(true, -1); 
        subg.DelLast();

        if(original)
        {
            subgraphCounter++;
        }
        else
        {
            randomCounter++;
        }
    }
}

//determine valid edges in subgraph
void getAdjacenciesOfNodesInSubgraphUndirected2(const PUNGraph& graph,
                                                const TVec<TInt>& accumulated, 
                                                const TVec<TInt>& subgraph, 
                                                const TVec<TInt>& Extension,
                                                TVec<TInt>& currNodeAdj,
                                                int targetNode, 
                                                int G_N,  int currNode)
{
  const TUNGraph::TNodeI NI = graph->GetNI(currNode);

  int deg = NI.GetOutDeg();
  //determine if each edge is valid and add to edge list
  for(int j = 0; j < deg; j++)
  {
    int outNodeId = NI.GetOutNId(j);//current edge 

    if(targetNode < outNodeId)
    {
      for( int k=0; k< subgraph.Len(); k++)
      {
        if( ! graph->IsEdge(subgraph[k], outNodeId))
        {
           if( !TVecContains(subgraph, outNodeId) &&
               !TVecContains(Extension, outNodeId) && 
               !TVecContains(currNodeAdj, outNodeId) &&
               !TVecContains(accumulated, outNodeId))       
           {

             currNodeAdj.Add(outNodeId);
           }
        }
      }
    }
  }
}

static int c1 = 0;
//for Undirected graphs:for each node in graph adds qualifying adjacencies to the vectors Extension and VSubgraph
//base case: VSubgraph length =size k
void extendSubgraphUndirected(const PUNGraph& Graph, 
                                TVec<TInt> myAccumulated,
                                TVec<TInt>& VSubgraph, 
                                TVec<TInt>& Extension, 
                                set* gv, 
                                const int G_M,
                                const int G_N, 
                                graph* nautyGraph, 
                                int numNodes,
                                int* lab, 
                                int* ptn, 
                                set* theSet, 
                                int* orbits, 
                                optionblk& options,
                                statsblk& stats, 
                                setword* workspace, 
                                int maxM, 
                                graph* canon, int targetNode)
{
    c1 += 2;
   // for( int i=0; i<c1; ++i) cerr << " ";
   // cerr << "rec";
   // cerr << " SUB:"; printVector(VSubgraph);
    //cerr << " EXT:"; printVector(Extension);     

    TVec<TInt> myExtension = Extension;
    TVec<TInt> mySubgraph = VSubgraph;

    if(VSubgraph.Len() ==  G_N -1)
    {
   //cerr << " SUB:"; printVector(VSubgraph);
        printEnumeratedSubgraphs( mySubgraph,  myExtension, Graph, 
                                 gv, 
                                 G_M,
                                 G_N, 
                                 nautyGraph, 
                                 numNodes,
                                 lab, 
                                 ptn, 
                                 theSet, 
                                 orbits, 
                                 options,
                                 stats, 
                                 workspace, 
                                 maxM, 
                                 canon); 
   // cerr << "FOUND: "; printVector(mySubgraph);
      //  cerr << endl;
        c1 -=2;
        return;
    } 
    //cerr << endl;        
   

    TVec<TInt> accumulated = myAccumulated;
    
    //verify if all edges will qualify and add to edge list
    while(!myExtension.Empty())
    {
//static int blah = 0;
//if(blah++ > 10) return;    
     // for( int i=0; i<c2; ++i) cerr << " ";
     // cerr << "while" << endl;     
      int currNode = myExtension[0];
      mySubgraph.Add(currNode); 
      accumulated.Add(currNode);
      myExtension.Del(0);
       
      TVec<TInt> currNodeAdj; 
      getAdjacenciesOfNodesInSubgraphUndirected2(Graph, accumulated, mySubgraph, 
                                                 myExtension,  currNodeAdj, 
                                                 targetNode, G_N, currNode);
      
        TVec<TInt> combinedExt = myExtension;
//cerr<< "HERE: "; printVector(currNodeAdj); cerr << endl;
        for(int i = 0; i < currNodeAdj.Len(); ++i)
        {
          combinedExt.Add(currNodeAdj[i]);
        }
      //  combinedExt.Sort();
   
        TVec<TInt> combinedSubg = mySubgraph;
        extendSubgraphUndirected(Graph,
                                accumulated,
                                combinedSubg, 
                                combinedExt, 
                                gv, 
                                G_M,
                                G_N, 
                                nautyGraph,                  
                                numNodes,
                                lab, 
                                ptn, 
                                gv, 
                                orbits, 
                                options, 
                                stats, 
                                workspace, 
                                maxM, 
                                canon, targetNode);
        mySubgraph.DelLast();
    }
    c1 -=2;
}

//enmuerate subgraphs in random graphs
void enumerateSubgraphsRand(const PUNGraph& Graph,
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{  
    TVec<TInt> VSubgraph;
    TVec<TInt> VExtension;
    probIter = 0;

    double randNum =  prob[probIter];
    double percent = randNum/100; 
    int randSubgraphEnum = percent * Graph->GetNodes() + 0.5;

    randNum =  prob[probIter];
    percent = randNum/100; 
    randSubgraphEnum = percent * Graph->GetNodes() + 0.5;

    TVec<TInt> nodesChosen;
    int count = 0; 

    probIter++;

    //get unique subset of nodes (50%) from orig graph and enumerate 
    while(count < randSubgraphEnum)
    {
                
        for(int i = 0; i < randomIds.Len(); i++)
        { 
            if(count < randSubgraphEnum)  
            {
                int digit = rand() % 2;
                if(digit)
                {
                    if(!nodesChosen.Empty())
                    {
                        for(int j = 0; j < nodesChosen.Len(); j++)
                        {
                            if(randomIds[i] == nodesChosen[j])
                            {
                                i++;
                                continue;
                            }
                        }
                    }
                    VSubgraph.Clr(true, -1);
                    VExtension.Clr(true, -1);
                    TSnap::DelSelfEdges(Graph);
                    const TUNGraph::TNodeI NI = Graph->GetNI(randomIds[i]);
                    int targetNode = NI.GetId();
                    VSubgraph.Add(targetNode);
                    nodesChosen.Add(targetNode);  
  
                    //iterate thru the targetNode's edges
                    for (int e = 0; e < NI.GetOutDeg(); e++)
                    {
                        if(NI.GetId() < NI.GetOutNId(e))
                        {
                            VExtension.Add(NI.GetOutNId(e)); 
                        }
                    }
                            
                    randNum =  prob[probIter];
                    percent = randNum/100; 
                    int randSubgraphEnum2 =  percent * VExtension.Len() + 0.5;
                            
                     while(VExtension.Len() > randSubgraphEnum2)
                     {
                
                            for(int i = 0; i < VExtension.Len(); i++)
                            { 
                               if(VExtension.Len() > randSubgraphEnum2) 
                               {       

                                   int digit = rand() % 2;
                                   if(digit)
                                   {   
                                        VExtension.Del(i);
                                   }   
                               } 
                           }
                       }
                       TVec<TInt> accumulated = VSubgraph;
                       extendSubgraphUndirected(Graph, accumulated, VSubgraph, VExtension,   
                                                gv, 
                                                G_M, 
                                                G_N, 
                                                nautyGraph, 
                                                numNodes, 
                                                lab, 
                                                ptn, 
                                                NILSET, 
                                                orbits, 
                                                options,
                                                stats, 
                                                workspace, 
                                                160 * MAXM, canon, targetNode);

                    count++;
                }
            }
        }
    }
}

//for Undirected graphs: adds qualifying nodes to the vectors Extension and VSubgraph
//base case: VSubgraph length =size k
void enumerateSubgraphsUndirected(const PUNGraph& Graph,
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{  
    TVec<TInt> VSubgraph;
    TVec<TInt> VExtension;
    probIter = 0;
    double timeCalc = 0;
    int print = 0;
    
    //find every subgraph in original graph
    for (TUNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) 
    {
        callCount++;
        timeCalc = (numNodes * .10);
        int total = timeCalc + 0.5;
        print = total;

        //if original graph print progress update to stdout
        if(original)
        {    
            if(total > 0)
            {
                std::vector<int> myVec;
               // myVec.push_back(0);
                LabelNodeIdMap[NI.GetId()] = myVec;
                
                if(callCount %  total == 0)
                {
                    print += total;
                    cerr<< "Original Graph Total Progress: " << callCount << "/" << numNodes << endl;
                }
            }
            if(callCount == numNodes)
            {
                cerr<< "Original Graph Total Progress Complete "  << endl;
            }
        }
        VSubgraph.Clr(true, -1);
        VExtension.Clr(true, -1);
        TSnap::DelSelfEdges(Graph);
        int targetNode = NI.GetId();
        VSubgraph.Add(targetNode);
        const TUNGraph::TNodeI NI = Graph->GetNI(targetNode);

       //iterate thru the targetNode's edges
        for (int e = 0; e < NI.GetOutDeg(); e++)
        {
            if(NI.GetId() < NI.GetOutNId(e))
            {
                VExtension.Add(NI.GetOutNId(e)); 
            }
        }
        TVec<TInt> accumulated = VSubgraph;
        extendSubgraphUndirected(Graph, accumulated, VSubgraph, VExtension,   
                                gv, 
                                G_M, 
                                G_N, 
                                nautyGraph, 
                                numNodes, 
                                lab, 
                                ptn, 
                                NILSET, 
                                orbits, 
                                options,
                                stats, 
                                workspace, 
                                160 * MAXM, canon, targetNode);  
    }
}

//generate specified num of rand graphs and call enumeration func
void randomUndirected(PUNGraph& Graph,  
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{
    long progress = 0;
    int print = 0;
    //double total = 0;
    long mtime = 0;
 
    while(networkCount < networks)
    {
        countSubgrPtr++;
        labelCountPtr++;

        gettimeofday(&start1, NULL);
        TIntV resultDegree;
        //get orignal graph deg sequence vector
        TSnap::GetDegSeqV(Graph, resultDegree);
        TRnd rand = (time(NULL), 0);
        PUNGraph graph = dirMethod(resultDegree, rand);
        TSnap::DelSelfEdges(graph);
        gettimeofday(&end1, NULL);
        long seconds  = end1.tv_sec  - start1.tv_sec;
        long useconds = end1.tv_usec - start1.tv_usec;
        mtime += ((seconds *1000 ) + (useconds/1000.0));

        for (TUNGraph::TNodeI NI = graph->BegNI(); NI < graph->EndNI(); NI++) 
        {
            randomIds.Add(NI.GetId());    
        }

        enumerateSubgraphsRand(graph, 
                                         gv, 
                                         G_M, 
                                         G_N, 
                                         nautyGraph, 
                                         numNodes, 
                                         lab, 
                                         ptn,
                                         NILSET, 
                                         orbits, 
                                         options,
                                         stats, 
                                         workspace, 
                                         160 * MAXM, 
                                         canon);
        networkCount++;
        randCount++;
        progress = (networks * .10);
        int total = progress + 0.5;
        print = total;
            
        if(total > 0)
        {
            if(networkCount %  total == 0)
            {
                print += total;
                cerr<< "Random Graph Total Progress: " << networkCount << "/" << networks << endl;
            }
        }

        if(networkCount == networks)
        {
            cerr<< "Random Graph Total Progress Complete "  << endl;
        }
    }
    randTime = double(mtime);
}

//nemoCollection: generate label subgraphs for signicant motifs
void generateLabelSubgraphs(PUNGraph& Graph,  
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM,
                                    graph* canon)
{
    TVec<TInt> nodeList;

    //to generate subgraphs for each sig motif and then enumerate the subgraphs
    if(s_nemoCollection == "y")
    {
        //if rand graphs determined sig motifs
        if(labelId.size() > 0)
        {
            //get sig motif node id's  from nodeIdMap and add to nodeList 
            for(unsigned int i = 0; i < labelId.size(); i++)
            {
                std::map<graph64, map<int, int> >::iterator iter = nodeIdMap.find( labelId[i]);
                std::map<int, int> myMap = iter->second;
                std::map<int, int>::iterator innerIter = myMap.begin();

                //add node ids to nodeList to populate the subgraph
                for( ; innerIter != myMap.end(); innerIter++)
                {
                    nodeList.Add(innerIter->first);
                }

                PUNGraph graph = TSnap::GetSubGraph(Graph, nodeList);
                nodeList.Clr();
                enumerateSubgraphsUndirected(graph, 
                                         gv, 
                                         G_M, 
                                         G_N, 
                                         nautyGraph, 
                                         numNodes, 
                                         lab, 
                                         ptn,
                                         NILSET, 
                                         orbits, 
                                         options,
                                         stats, 
                                         workspace, 
                                         160 * MAXM, 
                                         canon);
                labelIter++;
            }
        }
    }
} 
//////END UNDIRECTED FUNCTIONS///////////////////////////////////////////



//BEGIN DIRECTED FUNCTIONS///////////////////////////////////////////
//called for each node in the subgraph to find it's edges within VSubgraph
void setEdgesDirected(int targetNode, const PNGraph& Graph,  TVec<TInt>& VSubgraph, set* gv, const int G_M, graph* nautyGraph)
{

    PNGraph subgraph = TSnap::GetSubGraph(Graph, VSubgraph);//make subgraph from VSubgraph
    //TVec<TInt> edges;

    //for(int i = 0; i < VSubgraph.Len(); i++)
    //{         
        //edges.Add(subgraph->IsEdge(VSubgraph[targetNode], VSubgraph[i]));

    //}

        //call ADDELEMENT for each edge 
    for(int i = 0; i < VSubgraph.Len(); i++)
    {

        if(subgraph->IsEdge(VSubgraph[targetNode], VSubgraph[i]))
        {      

            //NAUTY calls
            DELELEMENT(GRAPHROW(nautyGraph, targetNode, G_M), i);
            gv = GRAPHROW(nautyGraph, targetNode, G_M); 
            ADDELEMENT(gv, i);//call on ith element to set(targetNode) bit = 1 in gv
        }
    }
    //edges.Clr();        
}


//find nodes call addelement on for each graphrow call
void editNautyGraphDirected(int graphSet, const PNGraph& Graph,
                    TVec<TInt>& VSubgraph, 
                    const int G_M, 
                    set* gv, 
                    const int G_N,
                    graph* nautyGraph, 
                    int numNodes, 
                    int* lab, 
                    int* ptn, 
                    set* theSet, 
                    int* orbits, 
                    optionblk& options,
	                statsblk& stats, 
                    setword* workspace, 
                    int maxM, 
                    graph* canon)
{

    setEdgesDirected(graphSet, Graph, VSubgraph, gv, G_M, nautyGraph);
}


//finds the target node's edges in Directed graph
void GetAdjacenciesOfNodesInSubgraphDirected(const PNGraph& graph, 
                                     const TVec<TInt>& subgraph, 
                                     TVec<TInt>& extension,
                                     int targetNode, int currNode, const TVec<TInt>& VExtension)
{  

    int isValid = true;

    for(int i = 0; i < VExtension.Len(); i++)
    {
        extension.Add(VExtension[i]);
    }

    //for (TNGraph::TNodeI NI = graph->BegNI(); NI < graph->EndNI(); NI++) 
    //{
    TSnap::DelSelfEdges(graph);

    const TNGraph::TNodeI NI = graph->GetNI(currNode);
    int outDeg = NI.GetOutDeg();
    int inDeg = NI.GetInDeg();
    int id = 0;


    for(int i = 0; i < outDeg; i++)
    {
            
        id = NI.GetOutNId(i);
        isValid = true;
        if(id > targetNode)
        {

            if( ! TVecContains(subgraph, id) &&
                ! TVecContains(extension, id ) )
            {
                for(int j = 0; j < subgraph.Len(); j++)
                {        
                    if(currNode != subgraph[j])
                    {      
                        int sub = subgraph[j];
                        if(graph->IsEdge(sub, id ) || graph->IsEdge(id, sub ) )
                        {
                            isValid = false;
                        } 
                     }                                                        
                 }  
                
                 if(isValid)
                 {
                    extension.Add(id);  
                           
                 }
            }
        }
    }


    for(int i = 0; i < inDeg; i++)
    {
            
        id = NI.GetInNId(i);
        isValid = true;
        if(id > targetNode)
        {

            if( ! TVecContains(subgraph, id) &&
                ! TVecContains(extension, id ) )
            {

                for(int j = 0; j < subgraph.Len(); j++)
                {        
                    if(currNode != subgraph[j])
                    {      
                        int sub = subgraph[j];
                        if(graph->IsEdge(sub, id ) || graph->IsEdge(id, sub ) )
                        {
                            isValid = false;
                        } 
                     }                                                        
                 }  
                
                 if(isValid)
                 {

                    extension.Add(id);  
                           
                 }
            }
        }
    }

}


//for Directed graphs: adds qualifying nodes to the vectors Extension and VSubgraph
//base case: VSubgraph length =size k
void extendSubgraphDirected(const PNGraph& Graph, 
                            TVec<TInt>& VSubgraph, 
                            TVec<TInt>& VExtension, 
                            //const TInt& graphSet, 
                            set* gv, 
                            const int G_M,
                            const int G_N,  
                            graph* nautyGraph, 
                            int numNodes,
                            int* lab, 
                            int* ptn, 
                            set* theSet, 
                            int* orbits, 
                            optionblk& options,
                            statsblk& stats, 
                            setword* workspace, 
                            int maxM, 
                            graph* canon, int targetNode)
{
    vector<int> id;
    //if a subgraph is found
    if(VSubgraph.Len() ==  G_N)
    {

        for(int i = 0; i < VSubgraph.Len(); i++)
        {
            id.push_back(int(VSubgraph[i]) );   
        }

        //printVector("FOUND==================: ", VSubgraph);


        //for each node in subgraph that corresponds to graphrow in nautyGraph call editNautyGraph
            for(int i = 0; i < G_N; i++)
            {
                editNautyGraphDirected(i, Graph, VSubgraph, G_M, gv, G_N, 
                                    nautyGraph, numNodes, 
                                    lab,  ptn, NILSET,  orbits, options,
                                    stats, workspace, 160 * MAXM,  canon);
            } 
        
        //NAUTY call for canonical labelling
        nauty(nautyGraph, lab, ptn, NILSET, orbits, &options, &stats, workspace, 160 * MAXM, G_M, G_N, canon);

        addToLabelCountHash(canon, gv, G_N, G_M, id);
        
        if(hashType == "subgraph" && !randGraph)
        { 
            //addToLabelNodeHash(canon, gv, G_N, G_M, id);
        }
 
        //after each subgraph clear the set data for each graphrow
        for (int i = 0; i != G_N; ++i) 
        {
            EMPTYSET(GRAPHROW(nautyGraph, i, G_M), G_M);
        }
        VSubgraph.DelLast();
        if(!randGraph)
        {
            subgraphCounter++;
        }
        else
        {
            randomCounter++;
        }
        return;
    }        

    while(VExtension.Len() != 0)
    {
        TVec<TInt> Extension; 

        int currNode = VExtension[0];
        VSubgraph.Add(currNode);
        VExtension.Del(0);

        GetAdjacenciesOfNodesInSubgraphDirected(Graph, VSubgraph, Extension, targetNode, currNode, VExtension);

        
        extendSubgraphDirected(Graph,
                                VSubgraph, 
                                Extension,                    
                                gv, 
                                G_M,
                                G_N, 
                                nautyGraph,                  
                                numNodes,
                                lab, 
                                ptn, 
                                gv, 
                                orbits, 
                                options, 
                                stats, 
                                workspace, 
                                maxM, 
                                canon, targetNode);
    }
    VSubgraph.DelLast();
}

void getEdge(int targetNode, TVec<TInt> VExtension, const PNGraph& Graph)
{
    int edge = 0;
    for (TNGraph::TNodeI NID = Graph->BegNI(); NID < Graph->EndNI(); NID++) 
    {
        edge = NID.GetId();

        if(edge > targetNode)
        {
            if(Graph->IsEdge(edge, targetNode) ||  Graph->IsEdge(targetNode, edge) )
            {
                VExtension.Add(edge);
                    
            }
        }
    }
}

//for DSirected graphs: adds qualifying nodes to the vectors Extension and VSubgraph
//base case: VSubgraph length =size k
void enumerateSubgraphsDirected(const PNGraph& Graph,
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{  

    TVec<TInt> VSubgraph;
    TVec<TInt> VExtension;

    int targetNode = 0;
    int isEdge = 0;

    for (TNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) 
    {
        callCount++;
            
        if((numNodes/10) == 0)
        {
            //cerr<< "Original Graph Total Progress: " << callCount << "/" << numNodes << endl;
        }

        VSubgraph.Clr(true, -1);
        VExtension.Clr(true, -1);
        targetNode = NI.GetId();
        VSubgraph.Add(targetNode);
        isEdge = 0;

        for (TNGraph::TNodeI NID = Graph->BegNI(); NID < Graph->EndNI(); NID++) 
        {
            isEdge = NID.GetId();

            if(isEdge > targetNode)
            {
                if(Graph->IsEdge(isEdge, targetNode) ||  Graph->IsEdge(targetNode, isEdge) )
                {
                    VExtension.Add(isEdge);
                        
                }
            }
        }

        extendSubgraphDirected(Graph, VSubgraph, VExtension,  

                                gv, 
                                G_M, 
                                G_N, 
                                nautyGraph, 
                                numNodes, 
                                lab, 
                                ptn, 
                                NILSET, 
                                orbits, 
                                options,
                                stats, 
                                workspace, 
                                160 * MAXM, canon, targetNode);  
    }
}


void randomDirected(const PNGraph& Graph, int numGraphs, 
                                    set* gv,
                                    const int G_M, 
                                    const int G_N, 
                                    graph* nautyGraph, 
                                    int numNodes, 
                                    int* lab, 
                                    int* ptn,
                                    set* theSet, 
                                    int* orbits, 
                                    optionblk& options,
                                    statsblk& stats, 
                                    setword* workspace, 
                                    int maxM, 
                                    graph* canon)
{
    for(int i = 0; i < numGraphs; i++)
    {
        TIntV resultDegree;
       
        //get orignal graph deg sequence vector
        TSnap::GetDegSeqV(Graph, resultDegree);
        TRnd rand = (Graph->GetNodes(), 0);
        PUNGraph graph = dirMethod(resultDegree, rand);
        PNGraph randGraph = TSnap::ConvertGraph<PNGraph, PUNGraph>( graph, false);

        enumerateSubgraphsDirected(randGraph, 
                                         gv, 
                                         G_M, 
                                         G_N, 
                                         nautyGraph, 
                                         numNodes, 
                                         lab, 
                                         ptn,
                                         NILSET, 
                                         orbits, 
                                         options,
                                         stats, 
                                         workspace, 
                                         160 * MAXM, 
                                         canon);
    }
}
//END DIRECTED FUNCTIONS///////////////////////////////////////////
/* 
void getAdjacencies(PUNGRAPH& Graph, TVec<TInt>& VSubgraph, TVec<TInt>& myExtension, int targetNode, const int  G_N, int currNode)
{

    const TUNGraph::TNodeI NI = graph->GetNI(currNode);
    int deg = NI.GetOutDeg();

    //iter over edges 
    for(int j = 0; j < deg; j++)
    {
        int outNodeId = NI.GetOutNId(j);//current edge 

        if(targetNode < outNodeId)
        {
            for(int k = 0; k < subgraph.Len(); k++)
            {   
                if(!graph->IsEdge(subgraph[k], outNodeId ))
                {
                    if( ! TVecContains(subgraph, outNodeId) &&
                        ! TVecContains(Extension, outNodeId) )
                    {
                        Extension.Add(outNodeId);  
                    }    
                }
            } 
        }
    }
}

void extend(const PUNGraph& Graph, 
                                TVec<TInt>& VSubgraph, 
                                TVec<TInt>& Extension, 
                                const int G_N, 
                                 int targetNode)
{

    TVec<TInt> myExtension;
    vector<int> id;

    //if a subgraph is found add to id vector for labelNodeIdMap
    if(VSubgraph.Len() ==  G_N)
    {
       for(int i = 0; i < VSubgraph.Len(); i++)
       
           id.push_back(int(VSubgraph[i]) );   
       }
        VSubgraph.DelLast();
    }         
    
        int currNode = 0;
        currNode = Extension[0];
        VSubgraph.Add(currNode);
        Extension.Del(0);
        
        for(int i = 0; i < Extension.Len(); i++)
        {
            myExtension.Add(Extension[i]);
        }

        getAdjacencies(Graph, VSubgraph, myExtension, targetNode, G_N, currNode);
        
    extendSubgraphUndirected(Graph,
                                VSubgraph, 
                                Extension, 
                                G_N, 
                                 targetNode);
    }
    VSubgraph.DelLast();
}



void enumerate(const PUNGraph& Graph,
                                    const int G_N) 
{  
    TVec<TInt> VSubgraph;
    TVec<TInt> VExtension;

    //if ORIGINAL graph or hashType = subgraphs
    if(!randGraph)
    {
        for (TUNGraph::TNodeI NI = Graph->BegNI(); NI < Graph->EndNI(); NI++) 
        {
            VSubgraph.Clr(true, -1);
            VExtension.Clr(true, -1);
            TSnap::DelSelfEdges(Graph);
            int targetNode = NI.GetId();
            VSubgraph.Add(targetNode);

            //iterate thru the targetNode's edges
            for (int e = 0; e < NI.GetOutDeg(); e++)
            {
                if(NI.GetId() < NI.GetOutNId(e))
                {
                    VExtension.Add(NI.GetOutNId(e)); 
                }
            }
        
            extend(Graph, VSubgraph, VExtension,   
                                G_N, 
                                 targetNode);  
        }
    }   
}


//+++++++++++++++++++++++++++++++++

*/

//main
int main(int argc, char* argv[])
{
    Env = TEnv(argc, argv, TNotify::StdNotify);
    TExeTm ExeTm;
    Try
    for(int i = 0; i < 8; i++)
    {
        prob.Add(50);
    }
    const TStr InFNm = Env.GetIfArgPrefixStr("-i:", "snapTestData.txt", "Input graph");
    const TStr GraphType = Env.GetIfArgPrefixStr("-g:", "Graph Type: Undirected", "Graph type");
    OutFNm = Env.GetIfArgPrefixStr("-o:", "output.txt", "Output file name prefix");
    const TInt subgSize = Env.GetIfArgPrefixInt("-s:", 3, "Subgraph size");
    const TStr nemoCollection = Env.GetIfArgPrefixStr("-n:", "Nemo Collection: n", "NemoCollection");   
    const TStr nemoCount = Env.GetIfArgPrefixStr("-c:", "Nemo Count: n", "NemoCount");   
    const TStr nemoProfile = Env.GetIfArgPrefixStr("-p:", "Nemo Profile: n", "NemoProfile");
    TStr nemoCountOutput = Env.GetIfArgPrefixStr("-co:", "nemoCount.txt", "NemoCount Outfile");
    const TStr nemoProfileOutput = Env.GetIfArgPrefixStr("-po:", "nemoProfile.txt", "NemoProfile Outfile");
    const TStr nemoCollectOutput = Env.GetIfArgPrefixStr("-no:", "nemoCollect.txt", "NemoCollect Outfile");
    const TStr subgraphCollection = Env.GetIfArgPrefixStr("-sn:", "Subgraph Collection: n", "Subgraph Collection");   
    const TStr subgraphCount = Env.GetIfArgPrefixStr("-sc:", "Subgraph Count: n", "Subgraph Count");   
    const TStr subgraphProfile = Env.GetIfArgPrefixStr("-sp:", "Subgraph Profile: n", "Subgraph Profile");
    const TStr subgraphCollectOutput = Env.GetIfArgPrefixStr("-sno:", "subgraphCollect.txt", "Subgraph Collect Outfile");
    const TStr subgraphProfileOutput = Env.GetIfArgPrefixStr("-spo:", "subgraphProfile.txt", "Subgraph Profile Outfile");
    const TStr subgraphCountOutput = Env.GetIfArgPrefixStr("-sco:", "subgraphCount.txt", "Subgraph Count Outfile");
    const TStr graphviz = Env.GetIfArgPrefixStr("-v:", "Motif Image: n", "GraphViz png");
    const TStr graphvizOutupt = Env.GetIfArgPrefixStr("-vo", ".png", "Graphviz output file");
    const TInt Networks = Env.GetIfArgPrefixInt("-r:", 1000, "Number of random graphs to generate");  
    int numNodes = 0;
    int numEdges = 0;
    s_nemoCollection = nemoCollection;
    s_nemoCount = nemoCount;
    s_nemoInputFile = InFNm;
    s_nemoProfile = nemoProfile;
    s_subgraphCollection = subgraphCollection;
    s_subgraphCount = subgraphCount;
    s_subgraphProfile = subgraphProfile;
    networks = Networks;
    inFile = InFNm;
    s_graphviz = graphviz;
    nemoCounting = nemoCountOutput;
    nemoProfile2 = nemoProfileOutput;
    nemoCollect = nemoCollectOutput;
    subgraphProfile2 = subgraphProfileOutput;
    subgraphCounting = subgraphCountOutput;
    subgraphCollect = subgraphCollectOutput; 
	bool directed = true;
	short G_N = subgSize;
    size = subgSize;
    srand(time(NULL));
    totalGraphs = Networks +1;
    initCountSubg();//initialize countSubg arr  
    initConcentration();//initialize concentration arr
   
    //set value for rand sig motif data collection and nodeid count  for each nauty label
    if(s_nemoCollection == "y" || s_nemoProfile == "y"  || s_nemoCount == "y" || s_subgraphProfile == "y" || s_subgraphCollection == "y")
    {
        hashType = "subgraph";
    }
    const int G_M = (G_N + WORDSIZE - 1) / WORDSIZE;
    graph canon[MAXN * MAXM];
    graph nautyGraph[MAXN * MAXM];
    for (int i = 0; i != G_N; ++i)
    {
		EMPTYSET(GRAPHROW(nautyGraph, i, G_M), G_M);
    }
	int lab[MAXN], ptn[MAXN], orbits[MAXN];
    static DEFAULTOPTIONS(options);
    statsblk(stats);
    setword workspace[160 * MAXM];
    set *gv = NULL;
    options.writeautoms = FALSE;
    options.getcanon = TRUE;
    if (directed)
    {
		options.digraph = TRUE;
		options.invarproc = adjacencies;
		options.mininvarlevel = 1;
		options.maxinvarlevel = 10;
    }
    nauty_check(WORDSIZE, G_M, G_N, NAUTYVERSIONID);
    cout << endl; 
    gettimeofday(&start, NULL);

    outfile = new std::ofstream(OutFNm.CStr());

    //
    if(GraphType == "directed")
    {
        cerr << "DIRECTED" << endl;
        PNGraph Graph = TNGraph::New();
        Graph = TSnap::LoadEdgeList<PNGraph>(InFNm); 
        TSnap::DelSelfEdges(Graph);
        numNodes = Graph->GetNodes();
        printf("nodes:%d  edges:%d\n", Graph->GetNodes(), Graph->GetEdges());
        cout << endl;

        enumerateSubgraphsDirected(Graph,                                   
                                    gv,  
                                    G_M, 
                                    G_N,   
                                    nautyGraph, 
                                    numNodes, 
                                    lab, 
                                    ptn, 
                                    NILSET, 
                                    orbits, 
                                    options,
                                    stats, 
                                    workspace, 
                                    160 * MAXM, 
                                    canon);

        
            printLabelNodeHash();
       

        if(Networks > 0)
        {
            randGraph = true;
            randomDirected(Graph, Networks,
                                         gv, 
                                         G_M, 
                                         G_N, 
                                         nautyGraph, 
                                         numNodes, 
                                         lab, 
                                         ptn,
                                         NILSET, 
                                         orbits, 
                                         options,
                                         stats, 
                                         workspace, 
                                         160 * MAXM, 
                                         canon);
        }
        printLabelCountHashDirected();
    }
    else
    {
        PUNGraph Graph = TUNGraph::New();
        Graph = TSnap::LoadEdgeList<PUNGraph>(InFNm);
        TSnap::DelSelfEdges(Graph);
        numNodes = Graph->GetNodes();
        numEdges = Graph->GetEdges();
        maxId = Graph->GetMxNId(); 
        printf("nodes:%d  edges:%d\n", numNodes, Graph->GetEdges());
        cout << endl;

        //enumerate subgraphs using fanmod algorithm
        enumerateSubgraphsUndirected(Graph,
                                    gv,  
                                    G_M, 
                                    G_N,  
                                    nautyGraph, 
                                    numNodes, 
                                    lab, 
                                    ptn, 
                                    NILSET, 
                                    orbits, 
                                    options,
                                    stats, 
                                    workspace, 
                                    160 * MAXM, 
                                    canon);


        original = false;

        
        //genrate rand graphs 
        if(Networks > 0)
        {
            randGraph = true;
            //genrate rand graphs and enumerate
            randomUndirected(Graph, 
                                 gv, 
                                 G_M, 
                                 G_N, 
                                 nautyGraph, 
                                 numNodes, 
                                 lab, 
                                 ptn,
                                 NILSET, 
                                 orbits, 
                                 options,
                                 stats, 
                                 workspace, 
                                 160 * MAXM, 
                                 canon);
        }
        randGraph = false;
        //print outfile.txt data (freq, pValue, zScore, etc)
        printLabelCountHash(Graph, G_N);


        gettimeofday(&end, NULL);
        long seconds  = end.tv_sec  - start.tv_sec;
        long useconds = end.tv_usec - start.tv_usec;
        long long mtime = ((seconds * 1000)  + useconds/1000.0) ;
        totalTime = double(mtime) /double(1000);

        TVec<TInt> edge;

        gettimeofday(&start3, NULL);

        //print nemoCollection data
        if(s_nemoCollection == "y")
        {
            nemoCollectionFile = new std::ofstream(nemoCollect.CStr());
            hashType = "none";

            //generate nauty labels for sig motifs
            generateLabelSubgraphs(Graph, 
                               gv, 
                               G_M, 
                               G_N, 
                               nautyGraph, 
                               numNodes, 
                               lab, 
                               ptn,
                               NILSET, 
                               orbits, 
                               options,
                               stats, 
                               workspace, 
                               160 * MAXM, 
                               canon);
            nemoCollectionFile->close();
            delete nemoCollectionFile;
        }
        gettimeofday(&end3, NULL);
        //time for subgraphCollection
        long seconds3  = end3.tv_sec  - start3.tv_sec;
        long useconds3 = end3.tv_usec - start3.tv_usec;
        long long mtime3 = ((seconds3 * 1000)  + useconds3/1000.0) ;
        nemoCollectionTime = double(mtime3) / double(1000); 


        //calc subgraph collection time
        gettimeofday(&start4, NULL);
        if(s_subgraphCollection == "y" )
        {
            printSubgraphIds();
        }

        //calc nemoProfile time
        gettimeofday(&start4, NULL);

        //print nemoProfile(labelNodeIdCount)
        if(s_nemoProfile == "y" || s_subgraphProfile == "y")
        {
            //print neMoProfile
            printLabelIdHash();
        }
        gettimeofday(&end4, NULL);
        long seconds4  = end4.tv_sec  - start4.tv_sec;
        long useconds4 = end4.tv_usec - start4.tv_usec;
        mtime4 = ((seconds4 * 1000)  + useconds4/1000.0) ;

        //calc nemoCounting time
        gettimeofday(&start5, NULL);

        //print nemoCounting file
        if(s_nemoCount == "y" || s_subgraphCount == "y" )
        {
            //print nemoCounting file
            printNemoCounting();
        }
        gettimeofday(&end5, NULL);
        long seconds5  = end5.tv_sec  - start5.tv_sec;
        long useconds5 = end5.tv_usec - start5.tv_usec;
        long long mtime5 = ((seconds5 * 1000)  + useconds5/1000.0) ;
        nemoCountTime = double(mtime5) / double(1000); //nemoCounting time
    }
 
    double rand = double(randTime) /double(1000) ;//seconds
    double graphvizTime = double(mtimeGraphviz) / double(1000); 
    double nemoProfileTime = double(mtime4) / double(1000);//seconds 
    int totalSubgraphCount = randomCounter + subgraphCounter;
    *outfile << "Input Graph: " <<  InFNm.CStr() << endl;
    *outfile << "GraphType:  " << GraphType.CStr() << endl;
    *outfile << "Nemo Collection: " << nemoCollection.CStr() << endl;   
    *outfile << "Nemo Count: " << nemoCount.CStr() << endl;   
    *outfile << "Nemo Profile: " <<  nemoProfile.CStr() << endl;
    *outfile << "Subgraph Collection: " <<  subgraphCollection.CStr() << endl;   
    *outfile << "Subgraph Count: " <<  subgraphCount.CStr() << endl;   
    *outfile << "Subgraph Profile: " << subgraphProfile.CStr() << endl;
    *outfile << "GraphViz Images: " << graphviz.CStr() << endl;
    *outfile << "Input file: " << InFNm.CStr() << endl;
    *outfile << "Nodes: " << numNodes << "  Edges: " << numEdges << endl;
    *outfile << "Subgraph size: " << subgSize << endl;
    *outfile << "Random Graphs Generated: " << Networks << endl; 
    *outfile << endl << subgraphCounter << " subgraphs were enumerated in the original network. " << endl; 
    *outfile << randomCounter << " subgraphs were enumerated in the random networks. " << endl; 
    *outfile << totalSubgraphCount << " subgraphs were enumerated in all networks. " << endl << endl;
    //total time includes orig and random enumeration and generation (and graphviz by default) 
    *outfile << "Total Time:  " << totalTime - graphvizTime << "  seconds." <<  endl;
    *outfile << "Randomization took " << rand  << " seconds." << endl;    
    *outfile << "Enumeration took " << totalTime - rand  - graphvizTime << "  seconds." <<  endl;
    *outfile << "NeMoCollection time:  " << nemoCollectionTime  << "  seconds." <<  endl;
    *outfile << "NeMoCounting time:  " << nemoCountTime  << "  seconds." <<  endl;
    *outfile << "NemoProfile Time: " <<  nemoProfileTime  << " seconds." <<  endl;
    *outfile << "Graphviz Time: " <<  graphvizTime  << " seconds." <<  endl;
   // *outfile << "Total time with nemoProfile: " <<  totalTime + nemoProfileTime - graphvizTime << "  seconds." <<  endl;
    *outfile << "Total time with graphViz, nemoProfile, nemoCounting, and nemoCollection: " <<  totalTime + nemoProfileTime + nemoCountTime + nemoCollectionTime << "  seconds." <<  endl;
    outfile->close();
    delete outfile;
    Catch
    return 0;
}

